// @ts-check

/** @type {any} */
var config;
try {
  /* @ts-ignore */
  config = /** @type {any} */ ((await import("/etc/app/config")).default);
}
catch (e) {
  config = /** @type {any} */ ((await import("./config-stub.js")).default);
}

config.port = config.port ?? 8080;
config.basePath = config.basePath ?? "";

export default /** @type {AppServer.Config} */ (config);
