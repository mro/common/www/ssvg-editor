
const
  { makeDeferred } = require("@cern/nodash");

/**
 * @template T
 * @typedef {import("@cern/nodash").Deferred<T>} Deferred<T>
 */

/**
 * @param {any} env
 */
async function createApp(env) {
  const Server = (await import("../../src/Server.js")).default;

  /** @type {Deferred<void>} */
  var def = makeDeferred();

  env.server = new Server({
    title: "", port: 0, basePath: ""
  });

  env.server.listen(() => def.resolve());
  return def.promise;
}

module.exports = { createApp };
