// @ts-check

import "./karma_index";
import { forEach } from "lodash";
import { describe, it } from "mocha";
import { expect } from "chai";

import { SSVGLinter } from "../src/utils/SSVGLint";
import d from "debug";

const debug = d("test");

describe("SSVGLinter", function() {
  [
    { doc: "<relation query-selector=\"div\" to=\"12\" />", diagnostics: [] },
    { doc: "<relation query-selector=\"div\" values=\"12;13\" />", diagnostics: [] },
    { doc: "<relation query-selector=\"div\" values=\"red;blue\" />", diagnostics: [] },
    { doc: "<relation query-selector=\"div\" values=\"'red';&quot;b;lue&quot;\" />", diagnostics: [] },

    { doc: "xxxx", diagnostics: [ "Error" ] },
    { doc: "<relation />", diagnostics: [ "query-selector", "values" ] },
    { doc: "<relation query-selector=\"div\" to=\"12\" attribute-type=\"XX\" />", diagnostics: [ "attribute-type" ] },
    { doc: "<relation query-selector=\"div\" values=\"'test';;\" />", diagnostics: [ "invalid string delimiter" ] },
    { doc: "<relation query-selector=\"div\" values=\"'test';12\" />", diagnostics: [ "invalid string delimiter" ] },
    { doc: "<relation query-selector=\"div\" values=\"10,12\" />", diagnostics: [ "invalid list separator" ] },
    { doc: "<relation query-selector=\"div\" values=\"12pt;13%\" />", diagnostics: [ "mixed units" ] },
    { doc: "<relation query-selector=\"div\" to=\"12pt\" from=\"12%\" />", diagnostics: [ "unit differs" ] },
    { doc: "<relation query-selector=\"div\" to=\"'test\" />", diagnostics: [ "unterminated string" ] },
    { doc: "<relation query-selector=\"div\" />", diagnostics: [ "attribute missing" ] },

    { doc: "<transform query-selector=\"div\" values=\"translate(10px); translate(10pt)\" />", diagnostics: [ "unit mismatch" ] },
    { doc: "<transform query-selector=\"div\" values=\"translate(10px); scale(0)\" />", diagnostics: [ "name mismatch" ] },
    { doc: "<transform query-selector=\"div\" />", diagnostics: [ "attribute missing" ] }
  ].forEach((t, i) => it(`generates diagnostics [${t.diagnostics.join(", ")}] (test ${i})`, function() {
    const linter = new SSVGLinter(t.doc);
    const diag = linter.lint();

    debug("diagnostics: %o", diag);
    forEach(t.diagnostics, (d, idx) => {
      expect(diag[idx]?.message).to.be.string(d);
    });
    expect(diag).to.have.length(t.diagnostics.length,
      "diagnostics message count mismatch");
  }));
});
