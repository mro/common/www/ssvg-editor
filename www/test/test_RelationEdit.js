// @ts-check

import "./karma_index";
import { get } from "lodash";
import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import { expect } from "chai";

import { createLocalVue, waitFor } from "./utils";

import RelationEdit from "../src/components/SSVGPropEdit/RelationEdit.vue";

describe("RelationEdit", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  /**
   * @param  {Tests.Wrapper<any>} wrapper
   */
  async function load(wrapper) {
    await wrapper.vm.$store.dispatch("engine/load", `
    <svg>
      <state>
        <property initial="0" max="3" min="0" name="range" type="number">
          <relation query-selector=".ssvg-shield path" attribute-name="fill" values="red;green;yellow;blue" />
        </property>
      <g class="ssvg-shield" style="transform: scale(0.8) rotateX(60deg) rotateZ(-45deg) translateZ(0px);">
        <path fill="#ffb13b" d="M300 154l-27 306-123 34-123-34L0 154z"></path>
        <path d="M42 315c-4-12-7-28-7-46 0-37 12-66 26-66 15 0 27 29 27 66H73c0-16-5-28-12-28-6 0-11 12-11 28 0 7 2 14 4 19s3 6 7 8c8 2 15 7 19 19 5 12 8 28 8 47 0 36-12 65-27 65-14 0-26-29-26-65h15c0 15 5 27 11 27 7 0 12-12 12-27 0-8-2-15-4-20s-5-6-8-8c-7-2-14-7-19-19z"></path>
        <path d="M99 315c-4-12-7-28-7-46 0-37 12-66 26-66 15 0 27 29 27 66h-15c0-16-5-28-12-28-6 0-11 12-11 28 0 7 2 14 4 19s3 6 7 8c8 2 15 7 20 19 4 12 7 28 7 47 0 36-12 65-27 65-14 0-26-29-26-65h15c0 15 5 27 11 27 7 0 12-12 12-27 0-8-2-15-4-20s-5-6-8-8c-7-2-14-7-19-19z"></path></g><g class="ssvg-shield-part" style="transform: scale(0.8) rotateX(60deg) rotateZ(-45deg) translateZ(0px);"><path fill="#ffd7a0" d="M249 440l24-261H150v289z"></path><path d="M199 203l-19 224h-16l-19-224h16l11 131 11-131h16z"></path>
        <path d="M226 296h27v66c0 36-12 65-27 65s-27-29-27-65v-93c0-37 12-66 27-66s27 29 27 66h-16c0-16-5-28-11-28s-11 12-11 28v93c0 15 5 27 11 27s11-12 11-27v-28h-11v-38z">
      </g>
    </svg>`);
    await waitFor(() => get(wrapper.vm, "$store.state.engine"));

    /** @type {HTMLElement} */
    const state = get(wrapper.vm, "$store.state.engine.stateElements").item(0);
    expect(state, "failed to find state").to.be.an("SVGElement"); // exist
    const property = /** @type {HTMLElement} */(
      state.getElementsByTagName("property").item(0));
    expect(property, "failed to find property").to.be.an("SVGElement");
    const relation = /** @type {HTMLElement|null} */(
      state.getElementsByTagName("relation").item(0));
    expect(relation, "failed to find relation").to.be.an("SVGElement");
    await wrapper.vm.$store.dispatch("selection/select", { state, property, relation });

    await waitFor(() => wrapper.vm.relation, "failed to load SSVG");
  }

  it("can mount RelationEdit", async function() {
    wrapper = mount(RelationEdit,
      { attachTo: document.body /* required to get elements bounding rect (drag/drop) */,
        localVue: createLocalVue() });
    await load(wrapper);
  });
});
