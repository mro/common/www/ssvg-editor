// @ts-check

import "./karma_index";
import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import { expect } from "chai";

import { createLocalVue, sortableDragDrop, waitForValue } from "./utils";

import PropertyEnumValuesEdit from "../src/components/SSVGPropEdit/PropertyEnumValuesEdit.vue";

describe("PropertyEnumValuesEdit", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it("can display enum", async function() {
    wrapper = mount(PropertyEnumValuesEdit, {
      propsData: { value: '"test";"titi";"toto"' },
      localVue: createLocalVue()
    });

    await waitForValue(() => wrapper.vm.valueList,
      [ "test", "titi", "toto" ]);
    wrapper.setProps({ value: '"test";"tata"' });
    await waitForValue(() => wrapper.vm.valueList,
      [ "test", "tata" ]);

    expect(wrapper.findAll("li.list-group-item")).to.have.length(2);

    wrapper.setProps({ value: null });
    await waitForValue(() => wrapper.vm.valueList, []);
  });

  it("can edit enum", async function() {
    wrapper = mount(PropertyEnumValuesEdit, {
      propsData: { value: '"test";"titi";"toto"' },
      attachTo: document.body /* required to get elements bounding rect (drag/drop) */,
      localVue: createLocalVue()
    });

    wrapper.setProps({ inEdit: true });
    await waitForValue(() => wrapper.vm.editValueList,
      [ "test", "titi", "toto" ]);
    wrapper.vm.add();
    wrapper.vm.removeAt(1);
    wrapper.findAll("li.list-group-item input").at(2).setValue("edited");
    await waitForValue(() => wrapper.vm.editValueList,
      [ "test", "toto", "edited" ]);

    wrapper.findAll("li.list-group-item .fa-times").at(1).trigger("click");
    await waitForValue(() => wrapper.vm.editValueList,
      [ "test", "edited" ]);

    wrapper.find(".fa-plus").trigger("click");
    await waitForValue(() => wrapper.vm.editValueList,
      [ "test", "edited", "" ]);

    await sortableDragDrop(
      wrapper.findAll("li.list-group-item").at(0).element,
      wrapper.findAll("li.list-group-item").at(2).element,
      "x-grip"
    );
    await waitForValue(() => wrapper.vm.editValueList,
      [ "edited", "", "test" ]);
  });
});
