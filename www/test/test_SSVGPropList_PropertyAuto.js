// @ts-check

import "./karma_index";
import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import { createLocalVue, waitForValue, waitForWrapper } from "./utils";
import { expect } from "chai";
import { get, set } from "lodash";

import App from "../src/App.vue";
import store from "../src/store";
import data from "./data/properties.svg";

describe("SSVGPropList", function() {
  describe("PropertyAuto", function() {
    /** @type {Tests.Wrapper} */
    let wrapper;

    afterEach(function() {
      if (wrapper) {
        wrapper.destroy();
        // @ts-ignore
        wrapper = null;
      }
    });

    it("can update auto property", async function() {
      wrapper = mount(App, { localVue: createLocalVue(), attachTo: document.body });

      let item = await waitForWrapper(
        () => wrapper.findComponent({ name: "SSVGItem" }), "failed to find SSVGItem");
      item.vm.loadDoc(data);

      item = await waitForWrapper(
        () => wrapper.findComponent({ name: "PropertyAuto" }), "failed to PropertyAuto");

      const input = item.find("input");
      const elt = /** @type {HTMLInputElement} */ (input.element);

      /* initial setup */
      store.commit("engine/updateState", { number: 0, enum: "initial", boolean: false, auto: false });
      await waitForValue(() => elt.value, "false", "widget value not updated", 4000);

      /* focus and edit */
      expect(input.exists(), "input not found").to.be.equal(true);
      /* can't really play with focus in unit-tests
        (if browser window doesn't have focus, focus() won't do anything) */
      set(item.vm, "$refs.value.hasFocus", true);
      input.setValue("\"test\"");

      await waitForValue(() => get(store, "state.engine.state.auto"),
        "test", "state value not updated");

      /* blur and update */
      set(item.vm, "$refs.value.hasFocus", false);
      store.commit("engine/updateState", { auto: 0 });
      await waitForValue(() => elt.value, "0", "widget value not updated");
    });
  });
});
