// @ts-check

import "./karma_index";
import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import { mapState } from "vuex";

import { createLocalVue, waitFor, waitForWrapper } from "./utils";

import App from "../src/App.vue";
import store from "../src/store";
import data from "./data/properties.svg";
import { BaseLogger } from "@cern/base-vue";
import d from "debug";
import { expect } from "chai";

const debug = d("test");

describe("App", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it("updates state only once", async function() {
    wrapper = mount({
      components: { App },
      template: "<App />",
      computed: {
        ...mapState("engine", [ "state" ])
      },
      /** @return {{ events: any[] }} */
      data() { return { events: [] }; },
      watch: {
        state(state) {
          debug("state event: ", state);
          /** @type {any[]} */(this.events).push(state);
        }
      }
    }, { localVue: createLocalVue(), attachTo: document.body });

    const item = await waitForWrapper(
      () => wrapper.findComponent({ name: "SSVGItem" }), "failed to find SSVGItem");

    item.vm.loadDoc(data);
    store.commit("engine/updateState", { number: 0, enum: "initial", boolean: false, auto: false });
    await waitFor(() => wrapper.vm?.state?.number === 0, "state not reset");

    wrapper.vm.events = [];
    store.commit("engine/updateState", { number: 100, enum: "explode", boolean: true, auto: true });
    await waitFor(() => wrapper.vm?.events?.length >= 1, "state not updated");

    await waitFor(() => wrapper.vm?.events?.length > 1, "ok", 500)
    .then(
      () => { throw "more than one event received"; },
      () => null);
  });

  it("handles merge errors", async function() {
    wrapper = mount(App, { localVue: createLocalVue(), attachTo: document.body });

    const item = await waitForWrapper(
      () => wrapper.findComponent({ name: "SSVGItem" }), "failed to find SSVGItem");
    await item.vm.loadDoc(data);

    const elts = await waitFor(() => wrapper.vm?.$store.state.engine?.stateElements, "engine not available");
    expect(elts).to.have.length(1, "invalid number of state elements");
    await wrapper.vm?.$store.dispatch("selection/select", {
      state: elts[0], property: elts[0].firstElementChild, relation: null
    });

    BaseLogger.clear();
    expect(BaseLogger.getErrors()).to.have.length(0, "errors not cleared");

    /* corrupted property */
    const ret = document.createElementNS("http://www.w3.org/2000/svg", "property");
    wrapper.vm?.$store.dispatch("add", ret);

    await waitFor(() => BaseLogger.getErrors().length > 0, "no errors reported");

    /* engine should be restored */
    await waitFor(() => wrapper.vm?.$store.state.engine?.stateElements, "engine not restored");
  });
});
