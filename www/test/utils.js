// @ts-check

import { createLocalVue as tuCreateLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";

import { defaultTo, get, has, toString } from "lodash";
import { makeDeferred } from "@cern/nodash";
import { expect } from "chai";

import BaseVue from "@cern/base-vue";
import Vuex from "vuex";

/**
 * @param  {() => boolean|any} cb
 * @param {string} [msg]
 * @param  {number} [timeout=1000]
 * @return {Promise<any>}
 */
export function waitFor(cb, msg = undefined, timeout = 1000) {
  var def = makeDeferred();
  var resolved = false;
  var err = new Error(msg || "timeout"); /* create this early to have stacktrace */
  /** @type {NodeJS.Timeout|null} */
  var nextTimer = null;
  var timer = setTimeout(() => {
    resolved = true;
    // @ts-ignore
    clearTimeout(nextTimer);
    def.reject(err);
  }, defaultTo(timeout, 1000));


  function next() {
    if (resolved) { return; }
    try {
      var ret = cb(); /* eslint-disable-line callback-return */
      if (ret) {
        clearTimeout(timer);
        resolved = true;
        def.resolve(ret);
      }
      else {
        nextTimer = /** @type {any} */(setTimeout(next, 200));
      }
    }
    catch (e) {
      clearTimeout(timer);
      resolved = true;
      def.reject(has(e, "message") ? e : new Error(String(e)));
    }
  }

  next();
  return def.promise;
}

/**
 * @template T=Wrapper
 * @param  {() => T} test
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Promise<T>}
 */
export function waitForWrapper(test, msg = undefined, timeout = undefined) {
  return waitFor(() => {
    const wrapper = test();
    // @ts-ignore
    return wrapper.exists() ? wrapper : false;
  }, msg, timeout);
}

/**
 * @param  {Function} fun
 */
function throws(fun) {
  try {
    fun();
    return null;
  }
  catch (e) {
    // @ts-ignore
    return e?.message ?? "failed";
  }
}

/**
 * @template T=any
 * @param  {() => T} test
 * @param  {any} value
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Promise<T>}
 */
export function waitForValue(test, value, msg = undefined, timeout = undefined) {
  /** @type {any} */
  var _val;
  return waitFor(() => {
    _val = test();
    return throws(() => expect(_val).to.deep.equal(value)) === null;
  }, msg, timeout)
  .catch((err) => {
    const assertMsg = throws(() => expect(_val).to.deep.equal(value)) ||
      ("Invalid result value: " + toString(_val) + " != " + value);
    if (err instanceof Error) {
      err.message = msg ? `${assertMsg}: (${msg})` : assertMsg;
      throw err;
    }
    throw new Error(msg);
  });
}

/**
 * @param  {HTMLElement|Vue.Component|Tests.Wrapper}  el
 * @return {boolean}
 * @details this method requires the Wrapper attached to document
 */
export function isVisible(el) { // eslint-disable-line complexity
  /** @type {HTMLElement|null} */
  var elt;
  if (has(el, "element")) {
    elt = get(el, "element"); /* vue wrapper */
  }
  else if (has(el, "$el")) {
    elt = get(el, "$el"); /* vue component */
  }
  else {
    // @ts-ignore
    elt = el;
  }
  if (!(elt instanceof Element)) { return false; }
  while (elt instanceof Element) {
    const style = window.getComputedStyle(elt);
    if (elt.hidden ||
      (style &&
       (style.visibility === "hidden" ||
        style.display === "none"))) {
      return false;
    }
    elt = get(elt, "parentElement");
  }
  return true;
}

export const TransitionStub = {
  template: "<div :is=\"tag\"><slot></slot></div>",
  props: { tag: { type: String, default: "div" } }
};

export const stubs = {
  "transition-group": TransitionStub,
  transition: TransitionStub
};

export function createLocalVue() {
  const local = tuCreateLocalVue();
  local.use(VueRouter);
  local.use(BaseVue);
  local.use(Vuex);
  return local;
}

/**
 * @param {Element} from
 * @param {Element} to
 * @param {string} [handle]
 */
export async function sortableDragDrop(from, to,
                                       handle = undefined,
                                       ghostClass = "list-group-item-primary") {
  const fromHandle = /** @type {Element} */ (handle ?
    from.getElementsByClassName(handle).item(0) : from);
  /* enter drag mode */
  fromHandle.dispatchEvent(new PointerEvent("pointerdown", { bubbles: true, cancelable: true }));
  fromHandle.dispatchEvent(new MouseEvent("mousedown", { bubbles: true, cancelable: true }));

  /* start to drag element using its handle */
  fromHandle.dispatchEvent(new DragEvent("dragstart", { bubbles: true, cancelable: true }));

  /* wait for sortable.js class (delayed) */
  await waitFor(() => from.classList.contains("sortable-chosen"),
    "item not selected for dragging");
  await waitFor(() => from.classList.contains(ghostClass),
    "item not ghosted");

  /* sortable uses x,y to ensure move is correct */
  const toRect = to.getBoundingClientRect();
  expect(toRect.y).to.be.gt(0,
    "failed to retrieve bounding rect, destination must be attached in DOM");
  to.dispatchEvent(new DragEvent("dragover",
    { bubbles: true, clientX: toRect.x + 1, clientY: toRect.y + 1 }));

  fromHandle.dispatchEvent(new DragEvent("dragend", { bubbles: true, cancelable: true }));

  to.dispatchEvent(new DragEvent("mouseup", { bubbles: true, cancelable: true }));
  to.dispatchEvent(new DragEvent("pointerup", { bubbles: true, cancelable: true }));
  await waitFor(() => !from.classList.contains("sortable-chosen"),
    "item not dropped");
}


