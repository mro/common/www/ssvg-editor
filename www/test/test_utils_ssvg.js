// @ts-check

import "./karma_index";
import { expect } from "chai";
import { describe, it } from "mocha";

import { parser } from "@cern/ssvg-engine";
import { trangeConvert, tvalueToString } from "../src/utils/ssvg";

describe("utils/ssvg", function() {
  it("serialize tvalue", async function() {
    [
      { value: "test", result: "\"test\"" },
      { value: "\"", result: "\"\"\"" }
    ].forEach((test) => {
      expect(tvalueToString(test.value, "string"))
      .to.equal(test.result, "failed to serialize tvalue");
    });
  });

  it("can convert TRange", function() {
    const range = /** @type {ssvg.$TRange} */ (parser.parseTRange("black;red;#112233;rgb(1,2,3)"));
    expect(range).to.be.an("object"); // exist

    /* string conversions */
    /** @type {ssvg.$TRange} */
    const strings = trangeConvert(range, "string");
    expect(strings).to.deep.equal({
      type: "string",
      values: [ "\"rgb(0, 0, 0)\"", "\"rgb(255, 0, 0)\"", "\"rgb(17, 34, 51)\"", "\"rgb(1, 2, 3)\"" ]
    });
    expect(trangeConvert(strings, "number")).to.deep.equal({
      type: "number",
      values: [ 0, 0, 0, 0 ] // data loss
    });
    expect(trangeConvert(strings, "color")).to.deep.equal({
      type: "color",
      values: [ "rgb(0, 0, 0)", "rgb(255, 0, 0)", "rgb(17, 34, 51)", "rgb(1, 2, 3)" ]
    });

    /* number conversions */
    /** @type {ssvg.$TRange} */
    let nums = trangeConvert(range, "number");
    expect(nums).to.deep.equal({
      type: "number",
      values: [ 0, 16711680, 1122867, 66051 ]
    });
    expect(trangeConvert(trangeConvert(nums, "string"), "number"))
    .to.deep.equal(nums);
    expect(trangeConvert(nums, "color")).to.deep.equal({
      type: "color",
      values: [ "#000000", "#ff0000", "#112233", "#010203" ]
    });

    /* unit is cleared */
    nums = { values: [ 1, 2 ], unit: "px", type: "number" };
    expect(trangeConvert(nums, "string"))
    .to.deep.equal({ type: "string", values: [ "\"1\"", "\"2\"" ] });
    expect(trangeConvert(nums, "color"))
    .to.deep.equal({ type: "color", values: [ "#000001", "#000002" ] });
  });
});
