// @ts-check

import "./karma_index";
import { transform } from "lodash";
import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import { expect } from "chai";

import { createLocalVue, waitFor, waitForWrapper } from "./utils";

import CreatePropertyDialog from "../src/components/SSVGPropList/CreatePropertyDialog.vue";

describe("CreatePropertyDialog", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  /**
   * @param  {Element} element
   */
  function getAttributes(element) {
    return transform(element.getAttributeNames(),
      (ret, name) => (ret[name] = element.getAttribute(name)),
      /** @type {{ [key: string]: string|null}} */({}));
  }

  it("can create a property", async function() {
    wrapper = mount(CreatePropertyDialog,
      { localVue: createLocalVue() });

    const prom = wrapper.vm.request();
    wrapper.find("input.x-name").setValue("test");
    wrapper.find("select.x-type").setValue("number");
    wrapper.find(".x-computed").vm.editValue = false;
    (await waitForWrapper(() => wrapper.find(".x-min")))
    .setValue("-42");
    wrapper.find(".x-max").setValue("54");
    wrapper.find(".x-initial").setValue("12");
    wrapper.findAll("button").filter((b) => (b.text() === "Ok"))
    .trigger("click");
    const ret = await prom;
    expect(ret).is.an("SVGElement");
    expect(ret.tagName).to.equal("property");
    expect(getAttributes(ret)).to.deep.equal({
      name: "test",
      type: "number",
      min: "-42", max: "54", initial: "12"
    });
  });

  it("can create an enum", async function() {
    wrapper = mount(CreatePropertyDialog,
      { localVue: createLocalVue() });

    const prom = wrapper.vm.request();
    wrapper.find("input.x-name").setValue("test");
    wrapper.find("select.x-type").setValue("enum");
    wrapper.find(".x-computed").vm.editValue = false;
    /** @type {Tests.Wrapper} */
    const enumEdit = await waitForWrapper(
      () => wrapper.findComponent({ name: "PropertyEnumValuesEdit" }),
      "failed to find EnumValuesEdit");
    enumEdit.vm.setAt(0, "test");
    enumEdit.vm.setAt(1, "titi");
    enumEdit.vm.add("toto");

    await waitFor(() => wrapper.findAll(".x-initial option")
    .filter((o) => (o.element.getAttribute("value") === "test")).exists());
    wrapper.find(".x-initial").setValue("test");
    wrapper.findAll("button").filter((b) => (b.text() === "Ok"))
    .trigger("click");
    const ret = await prom;
    expect(ret).is.an("SVGElement");
    expect(ret.tagName).to.equal("property");
    expect(getAttributes(ret)).to.deep.equal({
      name: "test",
      initial: "\"test\"",
      type: "enum",
      values: "\"test\";\"titi\";\"toto\""
    });
  });
});

