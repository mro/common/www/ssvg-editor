export default `<svg id="svg" height="40vh" width="100%" viewbox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
    <state>
        <transition duration="500ms" timing-function="ease-in-out" type="direct"></transition>
        <property name="state" type="enum" initial="'initial'" values="'initial';'explode';'align'">
            <transform query-selector="#rect1" values="translate(0);translate(-25 -25);translate(-30 0)"></transform>
            <transform query-selector="#rect2" values="translate(0);translate(-25 25);translate(-10 0)"></transform>
            <transform query-selector="#rect3" values="translate(0);translate(25 -25);translate(10 0)"></transform>
            <transform query-selector="#rect4" values="translate(0);translate(25 25);translate(30 0)"></transform>
            <relation query-selector="rect" attribute-name="fill" values="red;green;blue"></relation>
        </property>
    </state>
    <g transform="translate(50 50)">
        <rect id="rect1" y="-5" x="-5" width="10" height="10"></rect>
        <rect id="rect2" y="-5" x="-5" width="10" height="10"></rect>
        <rect id="rect3" y="-5" x="-5" width="10" height="10"></rect>
        <rect id="rect4" y="-5" x="-5" width="10" height="10"></rect>
    </g>
</svg>`;
