export default `<svg width="100%" xmlns="http://www.w3.org/2000/svg" height="40%">
  <state>
    <transition duration=".2"></transition>
    <property name="number" min="0" max="100" type="number" initial="0">
      <relation query-selector="#rect1" attribute-name="fill" to="blue"></relation>
    </property>
    <property name="enum" type="enum" initial="'initial'" values="'initial';'explode';'align'">
      <relation query-selector="#rect2" attribute-name="width" from="100%" to="0%" ></relation>
    </property>
    <property name="boolean" type="boolean" initial="false">
      <relation query-selector="#rect1" attribute-name="width" from="0%" to="100%" ></relation>
    </property>
    <property name="auto" type="auto" initial="false">
      <relation query-selector="#rect2" attribute-name="fill" to="yellow"></relation>
    </property>
  </state>
  <rect id="rect1" width="10%" height="20%" fill="rgb(0, 0, 0)" stroke="black" style="width: 0%;"></rect>
  <rect id="rect2" y="20%" width="10%" height="20%" fill="red" stroke="black" style="width: 100%;"></rect>
</svg>`;
