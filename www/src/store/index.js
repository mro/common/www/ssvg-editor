// @ts-check

import Vuex from "vuex";
import Vue from "vue";
import { forEach, isString, merge } from "lodash";
import createPersistedState from "vuex-persistedstate";
import d from "debug";
import {
  createStore,
  storeOptions
} from "@cern/base-vue";

import ui from "./modules/ui";
import {
  default as selection,
  plugin as selectionPlugin
} from "./modules/selection";
import loggerPlugin from "./modules/logger";
import engine from "./modules/engine";
import VuexHistory from "./modules/history";

import { setAttribute } from "../utils/element";

const debug = d("app:store");

Vue.use(Vuex);

/**
 * @typedef {V.ActionContext<AppStore.State>} ActionContext
 * @typedef {{
 *   stateIndex: number, propertyName: string, relationIndex: number,
 *   isTransform: boolean
 * }} SavedSelection
 */

/**
 * @param {Element|string} value
 */
function createFragment(value) {
  if (isString(value)) {
    // document.createRange().createContextualFragment(value) would use HTML NS
    const frag = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    frag.innerHTML = value;
    if (!frag || !frag.firstElementChild) { throw new Error(`invalid fragment: ${value}`); }
    return frag.firstElementChild;
  }
  return value;
}

/** @type {V.Store<AppStore.State>} */
merge(storeOptions, /** @type {V.Module<AppStore.State>} */({
  state: {
  },
  modules: {
    engine, selection, ui
  },
  actions: {
    /**
     * @brief add element to currently selected state
     * @param {Element|string} value
     */
    async add(context, value) {
      return Promise.resolve()
      .then(() => createFragment(value))
      .then((value) => {
        const frag = /** @type {Element} */ (context.state.engine.fragment?.cloneNode(true));
        const selection = context.getters["selection/getFragmentSelection"](frag);
        if (value.tagName === "property" || value.tagName === "computed") {
          if (!selection?.state) { throw new Error("no state selected"); }
          selection.state.append(value);
          debug("adding property");
        }
        else if (value.tagName === "transition") {
          if (!selection?.state) { throw new Error("no state selected"); }
          if (selection?.relation) {
            selection.relation.append(value);
          }
          else {
            selection.state.prepend(value);
          }
          debug("adding transition");
        }
        else {
          if (!selection?.property) { throw new Error("no property selected"); }
          selection.property.append(value);
          debug("adding relation");
        }
        return context.dispatch("engine/loadFragment", frag);
      });
    },
    /**
     * @brief remove currently selected property or relation
     * @param {"property"|"relation"|"transition"|"computed"} what
     */
    async remove(context, what) {
      return Promise.resolve()
      .then(() => {
        const frag = /** @type {Element} */ (context.state.engine.fragment?.cloneNode(true));
        const selection = context.getters["selection/getFragmentSelection"](frag);
        if (what === "property" || what === "computed") {
          if (!selection.property) { throw new Error("no property selected"); }
          debug("removing property");
          selection.property.remove();
        }
        else if (what === "relation") {
          if (!selection.relation) { throw new Error("no relation selected"); }
          debug("removing relation");
          selection.relation.remove();
        }
        else if (what === "transition") {
          if (!selection.transition) { throw new Error("no transition selected"); }
          debug("removing transition");
          selection.transition.remove();
        }
        else {
          throw new Error(`unknown remove request: ${what}`);
        }
        return context.dispatch("engine/loadFragment", frag);
      });
    },
    /**
     * @brief edit properties on currently selected property or relation
     * @param {{ what: "property"|"relation"|"transition"|"computed", values: any }} params
     */
    async edit(context, params) {
      return Promise.resolve()
      .then(() => {
        const frag = /** @type {Element} */ (context.state.engine.fragment?.cloneNode(true));
        const selection = context.getters["selection/getFragmentSelection"](frag);
        if (params.what === "property" || params.what === "computed") {
          if (!selection.property) { throw new Error("no property selected"); }
          forEach(params.values, (value, name) => {
            setAttribute(selection.property, name, value);
          });
        }
        else if (params.what === "relation") {
          if (!selection.relation) { throw new Error("no relation selected"); }
          forEach(params.values, (value, name) => {
            setAttribute(selection.relation, name, value);
          });
        }
        else {
          throw new Error(`unknown edit request: ${params.what}`);
        }
        debug("editing $o", params);
        return context.dispatch("engine/loadFragment", frag);
      });
    },
    /**
     *
     * @param {Element|string} value
     */
    async replace(context, value) {
      return Promise.resolve()
      .then(() => createFragment(value))
      .then((value) => {
        const frag = /** @type {Element} */ (context.state.engine.fragment?.cloneNode(true));
        const selection = context.getters["selection/getFragmentSelection"](frag);
        if (value.tagName === "property" || value.tagName === "computed") {
          if (!selection?.property) { throw new Error("no property selected"); }
          debug("replacing property");
          selection.property.replaceWith(value);
        }
        else if (value.tagName === "transition") {
          if (!selection?.transition) { throw new Error("no transition selected"); }
          debug("replacing transition");
          selection.transition.replaceWith(value);
        }
        else {
          if (!selection?.relation) { throw new Error("no relation selected"); }
          debug("replacing relation");
          selection.relation.replaceWith(value);
        }

        return context.dispatch("engine/loadFragment", frag);
      });
    }
  },
  plugins: [
    createPersistedState({
      key: `${window.location.pathname}/ssvg-editor/vuex`,
      paths: [ "ui.showKeyHints", "ui.viewMode", "engine.text", "selection.path" ],
      rehydrated(store) {
        store.dispatch("engine/init");
      }
    }),
    selectionPlugin,
    loggerPlugin
  ]
}));
const store = createStore();
export const History = new VuexHistory(store, "engine.text", 20);
History.on("item", (data) => store.dispatch("engine/load", data));

export default store;
