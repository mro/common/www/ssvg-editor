
import ssvg from "@cern/ssvg-engine";

export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface State {
    ui: UiState
    engine: SSVGEngine
    selection: Selection
  }

  interface UiState {
    showKeyHints: boolean,
    viewMode: string | null
  }

  interface SSVGEngine {
    text: string | null,
    fragment: Element | null,
    engine: ssvg | null,
    directEngine: ssvg | null,
    state: any | null,
    stateMin: any | null,
    stateMax: any | null,
    stateElements: HTMLCollectionOf<Element> | null,
    svgSelectors: string[] | null
  }

  interface Selection {
    state: Element | null,
    property: Element | null,
    relation: Element | null,
    transition: Element | null,
    path: [number, string | null, { index: number, tagName: string } | null, number | null] | null, // children path
    normalizedValue: any | null, // current value (normalized in [0, 1] range)
    keyTime: number | null, // current keyTime
    isOnKeyTime: boolean, // true if current normalizedValue is on keyTime
    keyTimes: number[] | null,
    tvalues: ssvg.$TRange | null
  }
}
