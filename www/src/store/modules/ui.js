// @ts-check
import Vue from "vue";
import { assign } from "lodash";

/** @type {V.Module<AppStore.UiState>} */
const mod = {
  namespaced: true,
  state: () => ({
    showKeyHints: false,
    viewMode: null
  }),
  mutations: {
    /**
     * @param  {AppStore.UiState} state
     * @param  {string} name
     */
    remove(state, name) {
      Vue.delete(state, name);
    },
    update: assign
  }
};
export default mod;
