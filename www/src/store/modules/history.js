// @ts-check

import { bindAll, cloneDeep, get, isNil } from "lodash";
import Vue from "vue";
import d from "debug";
import EventEmitter from "events";

const debug = d("app:store:history");

/**
 * @typedef {import("vuex").Store<any>} Store
 */

/**
 * @brief Vuex History class for Vue 2
 */
export default class VuexHistory extends EventEmitter {
  /**
   *
   * @param {Store} store
   * @param {string} property
   * @param {number} maxItems
   */
  constructor(store, property, maxItems) {
    super();
    /** @type {any[]} */
    this._data = [];
    this._current = -1;
    this._maxItems = maxItems;

    this._store = store;
    /** @type {(() => void)[]} */
    this.watches = [];

    this.state = Vue.observable({
      canUndo: false, canRedo: false
    });
    bindAll(this, [ "takeSnapshot", "undo", "redo", "clear" ]);

    const current = get(this._store.state, property);
    if (current) {
      this.takeSnapshot(current);
    }
    const w = this._store.watch((state) => get(state, property), this.takeSnapshot);
    this.watches.push(w);
  }

  /**
   * @param {any} value
   */
  takeSnapshot(value) {
    if (isNil(value)) {
      this.clear();
      return;
    }

    if (this._current >= 0 && value === this._data[this._current]) {
      debug("item already recorded");
      return;
    }

    debug("storing new item");
    this._data.length = ++this._current;
    this._data.push(cloneDeep(value));

    if (this._data.length > this._maxItems) {
      this._data.shift();
      debug("history buffer full, shifting oldest item");
    }
    this.state.canRedo = false;
    this.state.canUndo = this._current > 0;
  }

  undo() {
    if (this._current > 0) {
      debug("undo request");
      const data = this._data[--this._current];

      this.state.canRedo = true;
      this.state.canUndo = this._current > 0;
      this.emit("item", data);
    }
  }

  redo() {
    if (this._data && this._current < this._data.length - 1) {
      debug("redo request");
      const data = this._data[++this._current];

      this.state.canUndo = this._current > 0;
      this.state.canRedo = this._current < this._data.length - 1;
      this.emit("item", data);
    }
  }

  clear() {
    if (this._data.length !== 0) {
      debug("clear request");
      this._data.length = 0;
      this._current = -1;
      this.state.canRedo = false;
      this.state.canUndo = false;
    }
  }

  get canRedo() {
    return this.state.canRedo;
  }

  get canUndo() {
    return this.state.canUndo;
  }

  release() {
    this.watches.forEach((w) => w());
    this.watches = [];
  }
}
