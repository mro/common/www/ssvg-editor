// @ts-check

import Vue from "vue";
import { SSVGEngine } from "@cern/ssvg-engine";
import { find, forEach, get } from "lodash";
import { getDomSelectors } from "../../utils";

/** @type {V.Module<AppStore.SSVGEngine>} */
const mod = {
  namespaced: true,
  state: () => ({
    text: null, // complete svg as text
    fragment: null, // complete svg as Node tree
    engine: null, // SSVG engine
    state: null, // current ssvg state
    stateMin: null, // min values for states (auto type support)
    stateMax: null, // max values for states (auto type support)
    directEngine: null, // SSVG engine with no animations
    stateElements: null, // state elements list
    svgSelectors: null /* possible selectors for the document */
  }),
  getters: {
    svgSelectorsSet(state) {
      return new Set(state.svgSelectors);
    }
  },
  mutations: {
    /**
     * @param {AppStore.SSVGEngine} state
     */
    release(state) {
      if (state.engine) {
        state.engine.disconnect();
      }
      if (state.directEngine) {
        state.directEngine.disconnect();
      }
      forEach(state, (s, name) => {
        Vue.set(state, name, null);
      });
    },
    /**
     * @brief update the engine state
     * @param {AppStore.SSVGEngine} state
     * @param {any} value
     */
    updateState(state, value) {
      if (state.engine) {
        state.engine.updateState(value);
        mod?.mutations?.updateMinMax(state, value);
        /* update computed properties */
        state.state = state.engine.getState();
      }
      if (state.directEngine) {
        state.directEngine.updateState(value);
      }
    },
    /**
     * @brief set an new engine
     * @param {AppStore.SSVGEngine} state
     * @param {any} engine
     */
    updateEngine(state, engine) {
      state.engine?.disconnect();
      state.directEngine?.disconnect();

      state.fragment = engine.fragment;
      state.text = state.fragment?.outerHTML ?? null;
      state.engine = engine.engine;
      state.directEngine = engine.directEngine;
      state.svgSelectors = getDomSelectors(get(state.engine, [ "svg" ]));
      state.stateElements = state.engine.svg.getElementsByTagName("state");
      state.stateMin = null;
      state.stateMax = null;
      state.state = state.engine.getState();
    },
    /**
     * @brief update min/max values
     * @param {AppStore.SSVGEngine} state
     * @param {any} value
     * @details used to keep track of min/max for "auto" variables
     */
    updateMinMax(state, value) {
      state.stateMin = state.stateMin ?? {};
      state.stateMax = state.stateMax ?? {};
      forEach(value, (v, k) => {
        if (typeof v === "number") {
          if (v > (state.stateMax[k] ?? 1)) {
            state.stateMax[k] = v;
          }
          else if (v < (state.stateMin[k] ?? 0)) {
            state.stateMin[k] = v;
          }
        }
        else {
          state.stateMin[k] = state.stateMax[k] = null;
        }
      });
    }
  },
  actions: {
    /**
     * @param {string} text
     */
    async load(context, text) {
      return Promise.resolve()
      .then(() => {
        var frag = document.createRange().createContextualFragment(text);
        if (!frag) { throw "invalid XML"; }

        const element = find(frag.children, { nodeName: "svg" });
        if (!element || (element.nodeName !== "svg")) {
          throw new Error("not an SVG document");
        }
        return context.dispatch("loadFragment", element);
      });
    },
    /**
     * @param  {Element} element
     */
    async loadFragment(context, element) {
      const oldState = context.state.state;
      return Promise.resolve()
      .then(() => {
        const fragStates = element.getElementsByTagName("state");
        if (fragStates.length === 0) {
          element.insertBefore(
            document.createElementNS("http://www.w3.org/2000/svg", "state"),
            element.firstChild);
        }

        const engine = new SSVGEngine(/** @type {Element} */(element.cloneNode(true)));

        const directElement = /** @type {Element} */ (element.cloneNode(true));
        directElement.querySelectorAll("state transition").forEach((item) => {
          item.setAttribute("duration", "0");
          item.setAttribute("delay", "0");
        });
        return {
          fragment: Object.freeze(element),
          engine: Object.freeze(engine),
          directEngine: Object.freeze(new SSVGEngine(directElement))
        };
      })
      .then((value) => context.commit("updateEngine", value))
      .then(() => {
        if (oldState) { context.commit("updateState", oldState); }
      });
    },
    async reload(context) {
      const oldState = context.state.state;
      await context.dispatch("loadFragment", context.state.fragment);
      context.commit("updateState", oldState);
    },
    async init(context) {
      if (context.state.text) {
        return context.dispatch("load", context.state.text);
      }
    }
  }
};
export default mod;
