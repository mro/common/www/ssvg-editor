// @ts-check
import Vue from "vue";
import { assign, cloneDeep, defaults, findIndex, forEach, get, has, size } from "lodash";
import { getKeyTimes, getPropertyByName, getRelationValues } from "../../utils/ssvg";
import { getAttribute } from "../../utils/element";
import d from "debug";

const debug = d("app:store");

/** @type {V.Module<AppStore.Selection>} */
const mod = {
  namespaced: true,
  state: () => ({
    state: null,
    property: null,
    relation: null,
    transition: null,
    normalizedValue: null,
    path: null,
    keyTime: null,
    isOnKeyTime: false,
    keyTimes: null,
    tvalues: null,
    selectorHelper: null
  }),
  getters: {
    selectedElement(state) {
      if (state.transition) {
        return state.transition;
      }
      else if (state.relation) {
        return state.relation;
      }
      else {
        return state.property;
      }
    },
    isTransform: (state) => (
      get(state, [ "relation", "tagName" ]) === "transform"),
    computedKeyTimes: (state) => {
      if (state.keyTimes) {
        return state.keyTimes;
      }
      else if (state.tvalues && size(state.tvalues.values) >= 2) {
        const last = (state.tvalues.values.length - 1);
        return state.tvalues.values.map(
          (/** @type {any} */ val, /** @type {number} */ idx) => idx / last);
      }
      return (state.relation !== null) ? [ 0, 1 ] : null;
    },
    /**
     * @brief return the selection in given fragment
     * @return {(fragment: Element|null) => Partial<AppStore.Selection>|null}
     */
    getFragmentSelection(state) {
      /* eslint-disable-next-line complexity */
      return (/** @type {Element|null} */ fragment) => {
        if (!fragment || !state.path) { return null; }
        const elt = fragment.getElementsByTagName("state").item(state.path[0]);

        const property = state.path?.[1] ? getPropertyByName(elt, state.path[1]) : null;
        const relation = (property && state.path?.[2]) ?
          property.getElementsByTagName(state.path[2].tagName)
          .item(state.path[2].index) : null;
        let transition = (relation && state.path?.[3] !== null) ?
          relation.getElementsByTagName("transition")
          .item(state.path[3]) : null;
        if (!property && state.path?.[3] !== null) {
          transition = elt?.getElementsByTagName("transition")?.item(0) ?? null;
        }

        return { state: elt, property, relation, transition };
      };
    }
  },
  mutations: {
    /**
     * @param  {AppStore.Selection} state
     * @param  {string} name
     */
    remove(state, name) {
      Vue.delete(state, name);
    },
    /**
     * @brief set current selection
     * @param  {AppStore.Selection} state
     * @param  {any} values
     */ // eslint-disable-next-line complexity
    update(state, values) {
      assign(state, values);
      if (has(values, "relation")) {
        state.tvalues = values.relation ?
          getRelationValues(values.relation) : null;
        // @ts-ignore
        state.keyTimes = values.relation ?
          getKeyTimes(values.relation) : null;
        if (state.tvalues && !state.keyTimes) {
          state.keyTimes = state.tvalues.values.map(
            (val, idx) => idx / (state.tvalues.values.length - 1));
        }
      }
      var idx = 0;
      for (const k of (state.keyTimes || [])) {
        if (state.normalizedValue < k + 0.005) { break; }
        idx += 1;
      }
      state.keyTime = idx;
      state.isOnKeyTime =
        (Math.abs(get(state.keyTimes, [ state.keyTime ], 0) - state.normalizedValue) <= 0.005);
    },
    /**
     * @brief clear current selection
     * @param  {AppStore.Selection} state
     */
    clear(state) {
      forEach(state, (v, k) => {
        /** @type {{[k: string]: any}} */(state)[k] = null;
      });
    }
  },
  actions: {
    /**
     *
     * @param {{ state: Element|null, property: Element|null, relation: Element|null, transition: Element|null }} values
     */
    select(context, values) {
      /* ensure all parameters are set */
      defaults(values, { state: null, property: null, relation: null, transition: null });
      const stateIndex = findIndex(context.rootState.engine?.stateElements, (s) => s === values.state);
      const propertyName = getAttribute(values.property, "name");

      let relation = null;
      let transition = null;
      if (values.relation) {
        const tagName = values.relation?.tagName;
        const rels = values.property?.getElementsByTagName(tagName);
        const index = findIndex(rels, (r) => r === values.relation);
        if (index >= 0) {
          relation = { index, tagName: values.relation?.tagName };
          if (values.transition) {
            const trans = values.relation.getElementsByTagName("transition");
            transition = findIndex(trans, (r) => r === values.transition);
          }
        }
      }
      else if (!values.property && values.transition) {
        transition = 0;
      }
      const path = (stateIndex >= 0) ? [ stateIndex, propertyName ? propertyName : null, relation, transition ] : null;
      debug("selecting %o -> %o", values, path);
      context.commit("update", { ...values, path });
    },
    restore(context) {
      const selection = context.getters.getFragmentSelection(context.rootState?.engine?.engine?.svg);
      if (selection && context.state.path) {
        selection.path = [
          context.state.path[0],
          selection.property ? context.state.path[1] : null,
          selection.relation ? cloneDeep(context.state.path[2]) : null,
          selection.transition ? context.state.path[3] : null
        ];
        debug("restoring selection: %o -> %o", context.state.path, selection);
        context.commit("update", selection);
      }
      else {
        debug("restoring empty selection");
        context.commit("update", { relation: null, property: null, state: null, transition: null });
      }
    }
  }
};
export default mod;

/**
 * @param  {V.Store<AppStore.State>} store
 */
export function plugin(store) {
  store.watch((state) => get(state, [ "engine", "engine" ]),
    () => store.dispatch("selection/restore"));
}
