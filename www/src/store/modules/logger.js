// @ts-check
import { BaseLogger as logger } from "@cern/base-vue";

class VuexLogger {
  constructor() {
    this.lastError = null;
  }

  /** @param {any} err */
  error(err) {
    if (err !== this.lastError) {
      this.lastError = err;
      logger.error(err);
    }
  }
}

/**
 * @param {import("vuex").Store<any>} store
 */
function plugin(store) {
  const vuexLogger = new VuexLogger();
  store.subscribeAction({ error(action, state, error) { vuexLogger.error(error); } });
}

export default plugin;
