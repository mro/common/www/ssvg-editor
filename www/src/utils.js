// @ts-check

import { forEach, indexOf } from "lodash";

const DEFAULT_MAX_RESULTS = 250;

const renderableElements = new Set([
  "a", "circle", "ellipse", "foreignObject",
  "g", "image", "line", "path", "polygon", "polyline", "rect", "svg", "switch",
  "symbol", "text", "textPath", "tspan", "use"
]);

/**
 * @param  {Element} element
 * @param  {{ maxNodes?: number, maxResults?: number }} [options]
 * @return {string[]}
 */
export function getDomSelectors(element, options) {
  const ret = getDomIdClass(element, options);
  if (ret.length < (options?.maxResults ?? DEFAULT_MAX_RESULTS)) {
    const maxResults = (options?.maxResults ?? DEFAULT_MAX_RESULTS) - ret.length;
    return ret.concat(getDirectPaths(element, { maxResults }));
  }
  return ret;
}

/**
 * @param  {Element} element
 * @param  {{ maxNodes?: number, maxResults?: number }} [options]
 * @return {string[]}
 */
export function getDomIdClass(element, options) {
  if (!element) { return []; }

  const stack = [ element ];
  const ret = new Set();
  let maxNodes = options?.maxNodes ?? 250;
  let maxResults = options?.maxResults ?? DEFAULT_MAX_RESULTS;

  while (maxNodes > 0 && maxResults > 0 && stack.length > 0) {
    const element = /** @type {Element} */ (stack.shift());
    forEach(element.children, (elt) => { /* eslint-disable-line no-loop-func */ /* jshint ignore:line */
      stack.push(elt);
      if (elt.id) {
        ret.add("#" + elt.id);
        if (--maxResults <= 0) { return false; }
      }
      forEach(elt.classList, (c) => {
        ret.add("." + c);
        if (--maxResults <= 0) { return false; }
      });
      if (--maxNodes <= 0) { return false; }
    });
  }
  return Array.from(ret);
}

/**
 * @param  {Element} element
 * @param  {{ maxResults?: number, maxDepth?: number, allowedNodes?: Set<string> }} [options]
 * @return {string[]}
 */
export function getDirectPaths(element, options) {
  if (!element) { return []; }

  const stack = [ { elt: element, path: ":scope", depth: 0 } ];
  /** @type {string[]} */
  const ret = [];
  let maxResults = options?.maxResults ?? DEFAULT_MAX_RESULTS;
  const maxDepth = options?.maxDepth ?? 3;
  const allowedNodes = options?.allowedNodes ?? renderableElements;

  while (maxResults > 0 && stack.length > 0) {
    const element = stack.shift();
    /** @type {{ [tag: string]: number }} */
    const childCountMap = {};
    forEach(element?.elt.children, (elt) => { /* eslint-disable-line no-loop-func */ /* jshint ignore:line */
      const type = elt.tagName;
      if (!allowedNodes.has(type) || !element) { return; }

      childCountMap[type] = (childCountMap[type] ?? 0) + 1;

      const path = `${element?.path}>${type}:nth-of-type(${childCountMap[type]})`;
      if (element.depth < maxDepth) {
        stack.push({ elt, path, depth: element.depth + 1 });
      }
      ret.push(path);
      if (--maxResults <= 0) { return false; }
    });
  }
  return ret;
}

/**
 * @param {Element|null} root
 * @param {Element|null} element
 * @return {string|null}
 */
export function getElementPath(root, element) {
  if (element && element === root) {
    return ":scope";
  }
  else {
    if (!element || !element.parentElement) { return null; }
    const tagName = element.tagName;

    const nth = indexOf(element.parentElement.getElementsByTagName(tagName), element);

    return `${getElementPath(root, element.parentElement)}>${tagName}:nth-of-type(${nth})`;
  }
}

let id = 0;

/** @returns {string} */
export function genId() {
  return "x-" + (++id);
}

/**
 * @brief function used to automatically and correctly infer types in mixins
 * @template V, Data, Methods, Computed, PropNames, SetupBindings, Mixin, Extends
 * @type {V.mixinMaker<V, Data, Methods, Computed, PropNames, SetupBindings, Mixin, Extends>}}
 */
export function mixinMaker(m) { return /** @type {any} */ (m); }
