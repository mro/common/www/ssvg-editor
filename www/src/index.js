// @ts-check

import "./public-path";
import "./bootstrap-patch";

import Vue from "vue";
import store from "./store";
import App from "./App.vue";
import BaseVue from "@cern/base-vue";

Vue.use(BaseVue);

export default new Vue({
  el: "#app",
  extends: App,
  store
});
