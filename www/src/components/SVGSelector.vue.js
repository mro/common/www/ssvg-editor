// @ts-check

import Vue from "vue";
import { mapGetters, mapState } from "vuex";
import { find, get, isEmpty } from "lodash";
import { select } from "d3-selection";
import { makeDeferred } from "@cern/nodash";
import { BaseKeyboardEventMixin as KBMixin } from "@cern/base-vue";
import { getElementPath } from "../utils";
import d from "debug";

const debug = d("app:svg-selector");

/**
 * @typedef {import("d3-selection").Selection<Element|null, any, any, any>} Selection
 * @typedef {{ deferred: ReturnType<makeDeferred>|null }} Opts
 */

/**
 * @brief find and select element on SVG
 */
export default /** @type {V.Constructor<Opts, any>} */ (Vue).extend({
  name: "SVGSelector",
  mixins: [ KBMixin({ local: false }) ],
  computed: {
    ...mapState("engine", {
      /** @type {(state: AppStore.SSVGEngine) => Element|null} */
      directSvg: (state) => get(state, [ "directEngine", "svg" ]),
      /** @type {(state: AppStore.SSVGEngine) => Element|null} */
      svg: (state) => get(state, [ "engine", "svg" ])
    }),
    .../**
        * @type {{ selectorHelper(): string|null,
        *   path(): AppStore.Selection["path"] }}
        */(
      mapState("selection", [ "selectorHelper", "path" ])),
    .../**  @type {{ svgSelectorsSet(): Set<string> }} */(
      mapGetters("engine", [ "svgSelectorsSet" ]))
  },
  watch: {
    selectorHelper() {
      if (this.$options.deferred?.isPending && this.selectorHelper) {
        this.$options.deferred.resolve(this.selectorHelper);
      }
    },
    path() {
      /* on current selection change, abort any ongoing item selection */
      if (this.$options.deferred?.isPending) {
        debug("path changed, aborting selection");
        this.$options.deferred.resolve(null);
      }
    }
  },
  mounted() {
    this.onKey("esc", this._removeAll);
  },
  beforeDestroy() {
    this._removeAll();
  },
  methods: {
    /** @returns {Promise<string>} */
    async select() {
      const svgSelect = select(this.svg);
      const directSvgSelect = select(this.directSvg);

      svgSelect.call(this._hoverHook);
      directSvgSelect.call(this._hoverHook);
      this.$options.deferred = makeDeferred();

      /**
       * @param  {MouseEvent} event
       */ /* eslint-disable-next-line complexity */
      const selectItem = (/** @type {typeof svgSelect} */ selection, event) => {

        const root = /** @type {Element} */(selection.node());
        const target = /** @type {Element|null} */(
          this._findTarget(root, /** @type {Element|null} */(event.target)) ??
          event.target);
        let ret = this._getSelectClass(target);
        if (ret && this.$options.deferred) {
          return this.$options.deferred.resolve("." + ret);
        }
        ret = target?.getAttribute("id") ?? null;
        if (ret && this.$options.deferred) {
          return this.$options.deferred.resolve("#" + ret);
        }
        ret = getElementPath(root, target);
        if (ret && this.$options.deferred) {
          return this.$options.deferred.resolve(ret);
        }

        if (this.$options.deferred) {
          return this.$options.deferred.resolve(null);
        }
      };
      svgSelect.on("click.svgselector", (event) => selectItem(svgSelect, event));
      directSvgSelect.on("click.svgselector", (event) => selectItem(directSvgSelect, event));
      return this.$options.deferred.promise
      .then((res) => {
        this.$store.commit("selection/update", { selectorHelper: res });
        return /** @type {string}*/(res);
      })
      .finally(() => {
        this.$options.deferred = null;
        this._removeAll();
      });
    },
    _removeAll() {
      const svgSelect = select(this.svg);
      const directSvgSelect = select(this.directSvg);
      for (const e of [ "click", "mouseover", "mouseout" ]) {
        svgSelect.on(e + ".svgselector", null);
        directSvgSelect.on(e + ".svgselector", null);
      }
      svgSelect.selectAll(".hovered").classed("hovered", false);
      directSvgSelect.selectAll(".hovered").classed("hovered", false);
      if (this.$options.deferred) {
        const deferred = this.$options.deferred;
        this.$options.deferred = null;
        deferred.resolve(null);
      }
    },
    /**
     * @param  {Element|null} element
     */
    _getSelectClass(element) {
      if (!element || !this.svgSelectorsSet) { return null; }
      return find(element.classList,
        (c) => this.svgSelectorsSet.has("." + c)) || null;
    },
    /**
     * @param  {Element} root    [description]
     * @param  {Element|null} element [description]
     * @return {Element|null}         [description]
     */
    _findTarget(root, element) {
      if (!element) { return null; }

      const id = element.getAttribute("id");
      if (!isEmpty(id)) {
        return element;
      }
      const c = this._getSelectClass(element);
      if (!isEmpty(c)) {
        return element;
      }
      if (element !== root) {
        return this._findTarget(root, element.parentElement);
      }
      return null;
    },
    /**
     * @param  {Selection} selection [description]
     */
    _hoverHook(selection) {
      const self = this; // eslint-disable-line @typescript-eslint/no-this-alias
      const root = /** @type {Element} */(selection.node());
      selection.on("mouseover.svgselector", function(event) {
        const target = self._findTarget(root, event.target) ?? event.target;
        if (target) {
          select(target).classed("hovered", true);
        }
      })
      .on("mouseout.svgselector", function(event) {
        const target = self._findTarget(root, event.target) ?? event.target;
        if (target) {
          select(target).classed("hovered", false);
        }
      });
    }
  }
});

