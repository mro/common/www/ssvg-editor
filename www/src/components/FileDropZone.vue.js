// @ts-check

import Vue from "vue";
import { find, first, get, isNil, split } from "lodash";
import axios from "axios";

/**
 * @typedef {{ input: HTMLInputElement }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "FileDropZone",
  /**
   * @return {{
   *   dragover: boolean
   * }}
   */
  data() {
    return { dragover: false };
  },
  methods: {
    /**
     * @param {DragEvent} event
     */
    onDrop(event) {
      event.preventDefault();
      event.stopPropagation();
      this.dragover = false;

      /** @type {DataTransferItem|File|undefined} */
      var file = find(get(event, [ "dataTransfer", "items" ]), { kind: "file" });
      if (!isNil(file)) {
        this.$emit("file-drop", file.getAsFile());
        return;
      }

      file = get(event, [ "dataTransfer", "files", 0 ]);
      if (file) {
        this.$emit("file-drop", file);
        return;
      }

      file = find(get(event, [ "dataTransfer", "items" ]),
        { kind: "string", type: "text/uri-list" });
      if (!isNil(file)) {
        file.getAsString((uris) => {
          const url = first(split(uris, "\n"));
          if (url) {
            axios.get(url)
            .then(
              (ret) => this.$emit("text-drop", ret.data),
              (err) => console.log("failed to retrieve document:", err));
          }
        });
      }
    },
    /**
     * @param {Event} event
     * @param {boolean} dragover
     */
    onDrag(event, dragover) {
      event.preventDefault();
      event.stopPropagation();
      this.dragover = dragover;
    },
    /**
     * @param {Event} event
     */
    onDragOver(event) {
      event.preventDefault();
    },
    async onClick() {
      this.$refs?.input?.focus();
      this.$refs?.input?.click();
    },
    onFileChange() {
      const file = first(this.$refs?.input?.files);
      if (file) {
        this.$emit("file-drop", file);
      }
    }
  }
});
