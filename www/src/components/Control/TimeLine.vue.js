// @ts-check

import { clamp, defaultTo, get, invoke, map, throttle, transform } from "lodash";
import Vue from "vue";
import { mapGetters, mapState } from "vuex";
import { SSVGEngine, parser } from "@cern/ssvg-engine";
import { drag } from "d3-drag";
import { select } from "d3-selection";
import { scaleLinear } from "d3-scale";
import { bisect } from "d3-array";
import { BaseKeyboardEventMixin } from "@cern/base-vue";

import TimeLineMarker from "./TimeLineMarker.vue";
import { getAttribute } from "../../utils/element";

/**
 * @param {number[]} keyTimes
 * @param {any[]} values
 * @return {(x:number) => any}
 */
export function scaleThreshold(keyTimes, values) {
  return function(t) {
    return values[bisect(keyTimes, t, 1, keyTimes.length) - 1];
  };
}

/**
 * @typedef {import("d3-scale").ScaleLinear<number, number>} ScaleLinear
 * @typedef {import("d3-selection").Selection<Element, any, any, any>} Selection
 * @typedef {{
 *  ssvg: SSVGEngine
 *  arrow: Selection
 *  unscale: ((x: number) => boolean) | ScaleLinear
 *  scale: ((x: boolean) => number) | ((x: string) => number) | ScaleLinear
 * }} Opts
 */

export default /** @type {V.Constructor<Opts, any>} */ (Vue).extend({
  name: "SSVGTimeLine",
  components: { TimeLineMarker },
  mixins: [ BaseKeyboardEventMixin({ local: false }) ],
  /**
   * @return {{ dragging: boolean, isComputed: boolean }}
   */
  data() {
    return { dragging: false, isComputed: false };
  },
  computed: {
    .../** @type {{ state(): any, stateMin(): any, stateMax(): any }} */(
      mapState("engine", [ "state", "stateMin", "stateMax" ])),
    .../**
        * @type {{ property(): Element|null, relation(): Element|null,
        *   normalizedValue(): any, tvalues(): any }}
        */(
      mapState("selection", [
        "property", "relation", "normalizedValue", "tvalues"
      ])),
    .../** @type {{ keyTimes(): number[] }} */(
      mapGetters("selection", { keyTimes: "computedKeyTimes" })),
    /** @returns {string} */
    propertyName() {
      return getAttribute(this.property, "name");
    },
    /** @returns {string} */
    value() {
      return get(this.state, this.propertyName);
    },
    /** @returns {string} */
    type() {
      return getAttribute(this.property, "type");
    },
    /** @returns {number} */
    min() {
      if (this.type === "auto" && this.state) {
        return this.stateMin?.[this.propertyName] ??
          defaultTo(Number(getAttribute(this.property, "min")), 0);
      }
      else if (this.type === "number") {
        return defaultTo(Number(getAttribute(this.property, "min")), 0);
      }
      return 0;
    },
    /** @returns {number} */
    max() {
      if (this.type === "auto" && this.state) { // add a dependency on state
        return this.stateMax?.[this.propertyName] ??
          defaultTo(Number(getAttribute(this.property, "max")), 1);
      }
      else if (this.type === "number") {
        return defaultTo(Number(getAttribute(this.property, "max")), 1);
      }
      return 1;
    },
    /** @returns {{[name: string]: number}|null} */
    enumMap() {
      if (this.type === "enum") {
        const values = getAttribute(this.property, "values");
        /** @type {string[]} */
        const valuesList = parser.parseList({ index: 0, value: values },
          parser.parseString);
        return transform(valuesList,
          (ret, name, index) => (ret[name] = index / (valuesList.length - 1)),
          /** @type {{ [name:string]: number }} */ ({}));
      }
      return null;
    }
  },
  watch: {
    property() { this.reload(); },
    min() { this.reload(); },
    max() { this.reload(); },
    value() { this.updateState(); }
  },
  mounted() {
    this.$options.ssvg = new SSVGEngine(this.$el);

    this.$options.arrow = select("#arrow");
    this.$options.arrow.call(drag().on("drag", this.onArrowDrag));

    this.reload();
    this.updateState();
    this.onKey("right-keydown", () => this.nmove(0.005));
    this.onKey("left-keydown", () => this.nmove(-0.005));
    this.updateState = throttle(this.updateState, 15);
  },
  beforeDestroy() {
    if (this.$options.ssvg) {
      this.$options.ssvg.disconnect();
    }
    if (this.$options.arrow) {
      this.$options.arrow.on(".drag", null);
    }
  },
  methods: {
    /** @type {() => void} */
    reload() {
      this.isComputed = get(this.property, "tagName") === "computed";
      if (this.type === "boolean") {
        this.$options.unscale = (/** @type {number} */ x) => ((x >= 0.5) ? true : false);
        this.$options.scale = (/** @type {boolean} */ x) => (x ? 1 : 0);
      }
      else if (this.type === "enum") {
        this.$options.unscale = scaleThreshold(
          map(this.enumMap, (v) => v),
          map(this.enumMap, (v, k) => k));
        this.$options.scale = (/** @type {string} */ x) => (get(this.enumMap, x, 0));
      }
      else {
        this.$options.unscale = scaleLinear()
        .domain([ 0, 1 ]).range([ this.min, this.max ]).clamp(true);
        this.$options.scale = scaleLinear()
        .domain([ this.min, this.max ]).range([ 0, 1 ]).clamp(true);
      }
      this.updateState();
    },
    /**
     * @param  {MouseEvent} event
     */
    onArrowDrag(event) {
      if (!this.$options.arrow || this.isComputed) { return; }
      const x = clamp(event.x / get(this.$options.ssvg, [ "svg", "width", "baseVal", "value" ]), 0, 1);
      this.$store.commit("engine/updateState",
        { [this.propertyName]: this.$options.unscale(x) });
    },
    /** @type {() => void} */
    updateState() {
      const value =
        /** @type {(x: string) => number}*/(this.$options.scale)(this.value);
      this.$store.commit("selection/update", { normalizedValue: value });
      invoke(this, [ "$options", "ssvg", "updateState" ],
        { value: value, isComputed: this.isComputed });
    },
    /**
     * @param  {number} index
     */
    getValueAtIndex(index) {
      return get(this.tvalues, [ "values", index ]);
    },
    /**
     * @param  {number} value in normalized value from current position
     */
    nmove(value) {
      if (!this.$options.arrow || this.isComputed) { return; }
      this.$store.commit("engine/updateState", {
        [this.propertyName]: this.$options.unscale(this.normalizedValue + value)
      });
    },
    /**
     * @param  {number} value in normalized value to set
     */
    nset(value) {
      if (!this.$options.arrow || this.isComputed) { return; }
      this.$store.commit("engine/updateState", {
        [this.propertyName]: this.$options.unscale(value) });
    }
  }
});
