// @ts-check

import Vue from "vue";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "SSVGTimeLineMarker",
  props: {
    keyTime: { type: Number, default: 0 },
    value: { type: [ String, Number ], default: null }
  },
  methods: {
    /**
     * @param  {MouseEvent} event
     */
    onClick(event) {
      if (event) {
        event.preventDefault();
        event.stopPropagation();
      }
      this.$emit("select");
    }
  }
});
