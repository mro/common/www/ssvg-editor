// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import { json } from "@codemirror/lang-json";
import Card from "../Card.vue";
import CodeMirrorMixin from "../CodeMirrorMixin";
import { BaseLogger, BaseKeyboardEventMixin as KBMixin } from "@cern/base-vue";
import { isEqual } from "lodash";

/**
 * @typedef {{ card: V.Instance<Card>}} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "StateEdit",
  components: { Card },
  mixins: [ CodeMirrorMixin({ extensions: [ json() ] }), KBMixin({ local: true }) ],
  /** @return {{ showInfo: boolean }} */
  data() { return { showInfo: false }; },
  /**
   * @type {{
   *  state(): any, showKeyHints(): boolean, isModified(): boolean,
   *  editValueJson(): object|null
   * }}
   */
  computed: {
    ...mapState("engine", [ "state" ]),
    ...(mapState("ui", [ "showKeyHints" ])),
    isModified() {
      try {
        return !isEqual(this.editValueJson, this.state);
      }
      catch (e) {
        return false;
      }
    },
    editValueJson() {
      try {
        return JSON.parse(this.editValue);
      }
      catch {
        return null;
      }
    }
  },
  watch: {
    state(val) {
      if (this.isModified) {
        this.setContent(JSON.stringify(val, null, 2));
      }
    }
  },
  mounted() {
    this.setContent(JSON.stringify(this.state, null, 2));
    this.onKey("ctrl-s-keydown", (/** @type {Event}*/e) => {
      if (this.$refs.card?.isFocused) {
        e.preventDefault(); this.doApply();
      }
    });
  },
  methods: {
    /**
     * @type {() => void}
     */
    doApply() {
      try {
        this.$store.commit("engine/updateState", JSON.parse(this.editValue));
      }
      catch (err) {
        BaseLogger.error(err);
      }
    }
  }
});
export default component;
