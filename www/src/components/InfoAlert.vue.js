// @ts-check
import Vue from "vue";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({ name: "InfoAlert" });
