// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import { get, upperFirst } from "lodash";

import RelationEdit from "./SSVGPropEdit/RelationEdit.vue";
import TransformEdit from "./SSVGPropEdit/TransformEdit.vue";
import PropertyEdit from "./SSVGPropEdit/PropertyEdit.vue";
import TransitionEdit from "./SSVGPropEdit/TransitionEdit.vue";
import StateEdit from "./Control/StateEdit.vue";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "SSVGPropEdit",
  components: { RelationEdit, TransformEdit, StateEdit, PropertyEdit,
    TransitionEdit },
  filters: {
    /**
     * @param  {string} e
     * @return {string}
     */
    upperFirst(e) {
      return upperFirst(e);
    }
  },
  /**
   * @type {{
   *     property(): AppStore.Selection["property"],
   *     relation(): AppStore.Selection["relation"],
   *     path(): AppStore.Selection["path"],
   *     transition(): AppStore.Selection["transition"],
   *     tagName(): string,
   *     propertyKey(): string,
   *     stateKey(): string
   * }}
   */
  computed: {
    ...mapState("selection", [ "property", "relation", "path", "transition" ]),
    .../** @type {{ state(): any }} */mapState("engine", [ "state" ]),
    tagName() {
      return get(this.relation || this.property, [ "tagName" ], "");
    },
    propertyKey() {
      const p = this.path ?? [];
      return `${p[0]}-${p[1]}-${p[2]?.index}-${p[2]?.tagName}-${p[2]?.index}-${p[3]}`;
    },
    stateKey() {
      return `${this.path?.[0]}`;
    }
  }
});
