// @ts-check

import { Compartment, EditorState } from "@codemirror/state";
import { EditorView } from "@codemirror/view";
import { basicSetup } from "./CodeMirrorMixinInit";

import { mixinMaker } from "../utils";

/**
 * @typedef {{ cm: EditorView, extensions: any }} Opts
 * @typedef {{
 *  extensions?: import("@codemirror/state").Extension[]
 * }} CodeMirrorMixinOptions
 * @typedef {import("@codemirror/state").Transaction} CodeMirrorStateTransaction
 *
 * @typedef {import("vue").ComponentOptionsMixin} ComponentOptionsMixin
 * @typedef {{ value: string }} MixinProps
 * @typedef {{ editValue: string }} MixinData
 * @typedef {{
 *      _changeFilter(tr: CodeMirrorStateTransaction): boolean,
 *      setContent(data: string): void
 * }} MixinMethods
 * @typedef { Vue & MixinMethods & MixinData } MixinInstance
 * @typedef { MixinInstance & { $options: Opts } } MixinInstanceWithOptions
 */

const theme = EditorView.theme({
  "&": {
    fontSize: "0.875rem"
  }
});
export const ThemeCompartment = new Compartment();

const CodeMirrorMixin =
  (/** @type {CodeMirrorMixinOptions} */options) => mixinMaker({
    name: "CodeMirrorMixin",
    model: { prop: "value", event: "edit" },
    props: {
      value: { type: String, default: "" }
    },
    /** @return {MixinData} */
    data() {
      return { editValue: "" };
    },
    /** @this {MixinInstanceWithOptions} */
    beforeDestroy() {
      this.$options.cm.destroy();
    },
    /** @this {MixinInstanceWithOptions & Readonly<MixinProps>} */
    mounted() {
      const ext = [ basicSetup, EditorState.changeFilter.of(this._changeFilter),
                    ThemeCompartment.of(theme) ];
      if (options?.extensions) {
        ext.push(options.extensions);
      }

      this.$options.cm = new EditorView({
        extensions: ext,
        parent: (/** @type {Element} */(this.$refs?.cm) ?? this.$el),
        doc: this.value
      });
    },
    methods: {
      /**
       * @this {MixinInstance}
       * @param {CodeMirrorStateTransaction} tr
       * @returns {boolean}
       */
      _changeFilter(tr) {
        this.editValue = tr.state.doc.toString();
        this.$emit("edit", this.editValue);
        return true;
      },
      /**
       * @this {MixinInstance}
       * @returns {EditorView}
       */
      getEditor() {
        return /** @type {MixinInstanceWithOptions} */(this).$options.cm;
      },
      /**
       * @this {MixinInstance}
       * @param {string} data
       */
      setContent(data) {
        const cm = /** @type {MixinInstanceWithOptions} */(this).$options.cm;
        if (!cm) { return; }
        cm.dispatch(cm.state.update({
          changes: {
            from: 0, to: cm.state.doc.length, insert: data
          }
        }));
      }
    }
  });
export default CodeMirrorMixin;
