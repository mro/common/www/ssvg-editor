// @ts-check

import { get } from "lodash";
import Vue from "vue";
import { mapState } from "vuex";
import TimeLine from "./Control/TimeLine.vue";
import { getAttribute } from "../utils/element";
import InfoAlert from "./InfoAlert.vue";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "SSVGControl",
  components: { TimeLine, InfoAlert },
  data() { return { showInfo: false }; },
  /**
   * @type {{
   *  property(): Element|null, relation(): Element|null, state(): any,
   *  propertyName(): string, value(): any
   * }}
   */
  computed: {
    ...mapState("selection", [ "property", "relation" ]),
    ...mapState("engine", { state: (/** @type {AppStore.SSVGEngine} */state) => get(state, [ "state" ]) }),
    /**
     * @returns {string}
     */
    propertyName() {
      return getAttribute(this.property, "name");
    },
    value() {
      return get(this.state, this.propertyName);
    }
  }
});
