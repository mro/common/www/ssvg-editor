// @ts-check
/* eslint-disable max-lines */

import { get, invoke, noop } from "lodash";
import Vue from "vue";
import { mapState } from "vuex";
import FileDropZone from "./FileDropZone.vue";
import { BaseKeyboardEventMixin as KBMixin, BaseLogger as logger } from "@cern/base-vue";
import d from "debug";
import { History } from "../store";
import axios from "axios";

const debug = d("ssvg:editor");

/**
 * @typedef {{ ViewMode: ViewMode, History: History }} Opts
 * @typedef {{ svg: HTMLIFrameElement, svgShadow: HTMLIFrameElement }} Refs
 */

const ViewMode = {
  normal: "normal",
  shadow: "shadow",
  jump: "jump"
};

const iframeStyle = `
.hovered {
  cursor: crosshair!important;
  stroke: red!important;
  stroke-width: 2px!important;
  opacity: 1!important;
}
.x-ssvg-editor-selected {
  stroke: purple!important;
  stroke-width: 2px!important;
  opacity: 1!important;
}
body {
  overflow: clip;
  contain: paint;
}
`;

const component = /** @type {V.Constructor<Opts, Refs>} */ (Vue).extend({
  name: "SSVGItem",
  components: { FileDropZone },
  directives: {
    visible(el, binding) {
      el.style.visibility = binding.value ? "visible" : "hidden";
    }
  },
  mixins: [ KBMixin({ local: false }) ],
  ...{ ViewMode, History },
  /**
   * @return {{
   *   initTimer: NodeJS.Timeout|null,
   *   viewBox: string,
   *   zoom: { scale: 1, x: number, y: number },
   *   hasSession: boolean
   * }}
   */
  data() {
    return {
      initTimer: null,
      viewBox: "0 0 100 100",
      zoom: { scale: 1, x: 0, y: 0 },
      hasSession: false
    };
  },
  computed: {
    .../** @type {{directSvg(): SVGElement|null, svg(): SVGElement|null}} */(
      mapState("engine", {
        directSvg: (/** @type {AppStore.SSVGEngine} */state) => get(state, [ "directEngine", "svg" ]),
        svg: (/** @type {AppStore.SSVGEngine} */state) => get(state, [ "engine", "svg" ])
      })),
    /** @returns {string} */
    viewMode() { return this.$store?.state?.ui?.viewMode ?? ViewMode.shadow; },
    /** @return {boolean} */
    canUndo() { return History.canUndo; },
    /** @return {boolean} */
    canRedo() { return History.canRedo; },
    /** @return {boolean} */
    isZoom() {
      return this.zoom.x !== 0 ||
        this.zoom.y !== 0 ||
        this.zoom.scale !== 1;
    }
  },
  watch: {
    svg() { this.loadSvg(); },
    directSvg() { this.loadDirectSvg(); },
    viewBox() {
      this.updateViewBox(this.$refs.svg);
      this.updateViewBox(this.$refs.svgShadow);
    },
    zoom: {
      deep: true,
      handler() {
        this.updateViewBox(this.$refs.svg);
        this.updateViewBox(this.$refs.svgShadow);
      }
    }

  },
  mounted() {
    this.onKey("ctrl-z", History.undo);
    this.onKey("ctrl-y", History.redo);
    this.loadDocument();
    this.loadSvg();
    this.loadDirectSvg();
  },
  methods: {
    saveClipboard() {
      if (!navigator.clipboard) {
        logger.error("clipboard not available");
      }
      else {
        navigator.clipboard.writeText(this.$store.state?.engine?.text);
      }
    },
    saveFile() {
      const elt = document.createElement("a");
      elt.setAttribute("href", "data:image/svg+xml;charset=utf-8," +
        encodeURIComponent(this.$store.state?.engine?.text));
      elt.setAttribute("download", get(this.$store, [ "state", "engine", "fileName" ], "image"));
      document.body.appendChild(elt);
      elt.click();
      elt.remove();
    },
    saveSession() {
      if (!sessionStorage) {
        logger.error("session-storage not available");
        return;
      }
      try {
        const ssvg = JSON.parse(sessionStorage.getItem("ssvg") ?? "undefined");
        ssvg.document = this.$store.state?.engine?.text;
        sessionStorage.setItem("ssvg", JSON.stringify(ssvg));
        if (ssvg.callback) {
          window.location = ssvg.callback;
        }
      }
      catch (e) {
        logger.error(e);
      }

    },
    loadSvg() {
      if (this.initTimer) {
        clearTimeout(this.initTimer);
        this.initTimer = null;
      }

      const elt = /** @type {HTMLElement} */(this.getRootSvg(this.$refs.svg));
      if (!elt) { return; }
      this.clearElement(elt);
      if (this.svg) {
        debug("loading svg");
        elt.appendChild(this.svg);
        this.initDoc();
      }
      else {
        this.resetZoom();
      }
    },
    loadDirectSvg() {
      const elt =
        /** @type {HTMLElement} */(this.getRootSvg(this.$refs.svgShadow));
      if (!elt) { return; }

      this.clearElement(elt);
      if (this.directSvg) {
        elt.appendChild(this.directSvg);
      }
    },
    async loadDocument() {
      if (!await this.loadLocation()) {
        await this.loadSession();
      }
    },
    async loadSession() {
      try {
        const ssvg = JSON.parse(sessionStorage?.getItem("ssvg") ?? "undefined");
        if (ssvg && ssvg.document) {
          this.loadDoc(ssvg.document);
          this.hasSession = true;
          return true;
        }
      }
      catch (e) {
        console.warn("loadSession error: ", e);
      }
      return false;
    },
    async loadLocation() {
      try {
        const params = new URLSearchParams(document.location.search);
        const url = params.get("url");
        if (url) {
          await axios.get(url)
          .then(
            (ret) => {
              this.loadDoc(ret.data);
              // @ts-ignore: logger.info is checked
              logger?.info?.("external document loaded");
            },
            (err) => {
              logger.error("failed to retrieve external document: " + err.message);
              throw err;
            })
          .finally(() => {
            const url = new URL(document.location.href);
            url.searchParams.delete("url");
            window.history.pushState(null, "external document loaded", url.href);
          });
          return true;
        }
      }
      catch (e) {
        console.warn("loadLocation error: ", e);
      }
      return false;
    },
    onDirectSvgFrameLoaded() {
      this.initIFrame(this.$refs.svgShadow);
      this.loadDirectSvg();
    },
    onSvgFrameLoaded() {
      this.initIFrame(this.$refs.svg);
      this.loadSvg();
    },
    /** @param {HTMLIFrameElement} frame */
    initIFrame(frame) {
      const doc = get(frame, [ "contentDocument" ]);
      if (!doc || !doc.body) { return; }

      const svg = doc.createElementNS("http://www.w3.org/2000/svg", "svg");
      svg.setAttribute("width", "100%");
      svg.setAttribute("height", "100%");
      svg.setAttribute("preserveAspectRatio", "xMidYMid meet");
      svg.setAttribute("version", "1.1");
      svg.setAttribute("style", "overflow: visible;");

      svg.setAttribute("viewBox", this.viewBox);
      doc.body.appendChild(svg);

      /* add style for SVGSelector */
      /** @type {HTMLElement} */
      const style = doc.createElement("style");
      style.textContent = iframeStyle;
      doc.body.appendChild(style);

      /* handle svg navigation */ /* eslint-disable-next-line complexity */
      doc.body.onkeydown = (event) => {
        switch (event.key.toLowerCase()) {
        case "8":
        case "arrowup": this.zoom.y += 10; break;
        case "2":
        case "arrowdown": this.zoom.y -= 10; break;
        case "6":
        case "arrowright": this.zoom.x -= 10; break;
        case "4":
        case "arrowleft": this.zoom.x += 10; break;
        case "9":
        case "pageup":
        case "+": this.doZoom(true); break;
        case "3":
        case "pagedown":
        case "-": this.doZoom(false); break;
        }
      };
      doc.body.onpointerdown = (event) => {
        /* main button */
        if (event?.button !== 0) { return; }
        const pos = { x: -this.zoom.x + event.clientX, y: -this.zoom.y + event.clientY };
        doc.body.setPointerCapture(event.pointerId);
        doc.body.onpointermove = (event) => {
          if (pos) {
            this.zoom.x = event.clientX - pos.x;
            this.zoom.y = event.clientY - pos.y;
          }
        };
        doc.body.onpointerup = (event) => {
          doc.body.releasePointerCapture(event.pointerId);
          doc.body.onpointermove = null;
          doc.body.onpointerup = null;
        };
      };
      doc.body.onwheel = (event) => this.doZoom(event.deltaY < 0);
    },
    /** @param {boolean} zoomIn */
    doZoom(zoomIn) {
      if (zoomIn) {
        this.zoom.scale *= 1.1;
        this.zoom.x *= 1.1;
        this.zoom.y *= 1.1;
      }
      else {
        this.zoom.scale /= 1.1;
        this.zoom.x /= 1.1;
        this.zoom.y /= 1.1;
      }
    },
    resetZoom() {
      this.zoom = { x: 0, y: 0, scale: 1 };
    },
    /** @param  {HTMLIFrameElement} frame */
    updateViewBox(frame) {
      const svg = /** @type {HTMLElement} */(this.getRootSvg(frame));
      if (svg) {
        svg.setAttribute("viewBox", this.viewBox);
        svg.setAttribute("transform",
          `translate(${this.zoom.x}, ${this.zoom.y}) scale(${this.zoom.scale})`);
      }
    },
    /** @param  {HTMLIFrameElement} frame */
    getRootSvg(frame) {
      return get(frame,
        [ "contentDocument", "body", "firstChild" ]);
    },
    /** @param  {File} file */
    async onFileDrop(file) {
      this.loadDoc(await file.text());
    },
    /** @param  {string} text */
    async onTextDrop(text) {
      this.loadDoc(text);
    },
    /**
     * @param  {string} text
     */
    async loadDoc(text) {
      return this.$store.dispatch("engine/load", text).catch(noop);
    },
    /**
     * @param  {Element} elt
     */
    clearElement(elt) {
      while (elt && elt.firstChild) {
        elt.removeChild(elt.firstChild);
      }
    },
    initDoc() {
      this.initTimer = null;
      if (this.svg) {
        try {
          // const bbox = this.svg.getBBox();
          // if (!bbox.width || !bbox.height) { throw undefined; }
          // const width = get(this.svg, [ "width", "baseVal", "value" ], 100);
          // const height = get(this.svg, [ "height", "baseVal", "value" ], 100);
          // this.viewBox = `0 0 ${bbox.width} ${bbox.height}`;
          const width = get(this.svg, [ "width", "baseVal", "value" ], 100);
          const height = get(this.svg, [ "height", "baseVal", "value" ], 100);
          this.viewBox = `0 0 ${width} ${height}`;
        }
        catch (e) {
          this.initTimer = setTimeout(this.initDoc, 250);
        }
      }
    },
    doToggleViewMode() {
      if (this.viewMode === ViewMode.normal) {
        this.$store.commit("ui/update", { viewMode: ViewMode.shadow });
      }
      else if (this.viewMode === ViewMode.shadow) {
        this.$store.commit("ui/update", { viewMode: ViewMode.jump });
      }
      else {
        this.$store.commit("ui/update", { viewMode: ViewMode.normal });
      }
    },
    async doRelease() {
      if (await invoke(this.$refs["releaseDialog"], "request")) {
        this.$store.commit("engine/release");
        sessionStorage?.removeItem("ssvg");
      }
    },
    async doRefresh() {
      return this.$store.dispatch("engine/reload").catch(noop);
    }
  }
});
export default component;
