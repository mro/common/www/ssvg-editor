// @ts-check

import Vue from "vue";
import { clone } from "lodash";
import { parser } from "@cern/ssvg-engine";
import draggable from "vuedraggable";


const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "PropertyEnumValuesEdit",
  components: { draggable },
  props: {
    value: { type: String, default: null },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{ editValueList: string[]|null }}
   */
  data() {
    return { editValueList: null };
  },
  computed: {
    /** @return {string[]} */
    valueList() {
      try {
        return parser.parseList({ index: 0, value: this.value },
          parser.parseString);
      }
      catch {
        return [];
      }
    }
  },
  watch: {
    /** @param {string[]|null} value */
    editValueList(value) {
      this.$emit("edit", value);
    },
    /** @param {boolean} value */
    inEdit(value) {
      if (value) {
        this.editValueList = clone(this.valueList);
      }
    }
  },
  mounted() {
    this.editValueList = clone(this.valueList);
  },
  methods: {
    /**
     * @param  {number} idx
     */
    removeAt(idx) {
      if (this.editValueList) {
        this.editValueList.splice(idx, 1);
      }
    },
    /**
     * @param {string} [value=undefined]
     */
    add(value = undefined) {
      if (this.editValueList) {
        this.editValueList.push(value ? value : "");
      }
    },
    /**
     * @param {number} idx
     * @param {string} value
     */
    setAt(idx, value) {
      Vue.set(/** @type {object} */(this.editValueList), idx, value);
    }
  }
});

export default component;
