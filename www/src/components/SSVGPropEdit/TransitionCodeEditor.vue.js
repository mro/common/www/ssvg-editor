// @ts-check

import Vue from "vue";
import { mapState } from "vuex";

import { openLintPanel } from "@codemirror/lint";

import { ssvgElements, ssvgPrettyPrint } from "../../utils/SSVGLint";
import { noop } from "lodash";
import RelationCodeEditor from "./RelationCodeEditor.vue";

/**
 * @typedef {{ schema: typeof schema }} Opts
 * @typedef {{ cm: HTMLElement }} Refs
 */

const schema = [
  { ...ssvgElements.transition, top: true }
];

const component = /** @type {V.Constructor<Opts, Refs>} */(Vue).extend({
  name: "TransitionCodeEditor",
  extends: RelationCodeEditor,
  computed: {
    .../** @type {{ transition(): Element|null }} */(mapState("selection", [ "transition" ])),
    /** @return {string} */
    currentContent() {
      return (this.transition) ? ssvgPrettyPrint(this.transition.outerHTML) : "";
    }
  },
  mounted() {
    this.$options.schema = schema;
    this.updateLanguage();
  },
  methods: {
    async doApply() {
      if (!this.transition) { /* noop */ }
      else if (this.$refs.cm?.getElementsByClassName("cm-lint-marker-error").length > 0) {
        openLintPanel(this.getEditor());
        return false;
      }
      else {
        await this.$store.dispatch("replace", this.editValue).catch(noop);
        return true;
      }
    }
  }
});
export default component;
