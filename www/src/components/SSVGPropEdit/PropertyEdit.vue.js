// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import Card from "../Card.vue";
import PropertyCodeEditor from "./PropertyCodeEditor.vue";
import { BaseKeyboardEventMixin as KBMixin } from "@cern/base-vue";
import { noop } from "lodash";

/** @typedef {{ card: V.Instance<Card>, cm: V.Instance<PropertyCodeEditor>}} Refs */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "PropertyEdit",
  components: { Card, PropertyCodeEditor },
  mixins: [ KBMixin({ local: true }) ],
  props: { title: { type: String, default: "Property" } },
  computed: {
    .../** @type {{ property(): Element|null }} */(mapState("selection", [ "property" ])),
    .../** @type {{ showKeyHints(): boolean }} */(mapState("ui", [ "showKeyHints" ]))
  },
  mounted() {
    this.onKey("ctrl-s-keydown", (/** @type {Event} */e) => {
      if (this.$refs.card?.isFocused) {
        e.preventDefault(); this.doApply();
      }
    });
  },
  methods: {
    async doApply() {
      if (!this.property) { return; }
      this.$refs.cm?.doApply();
    },
    doDelete() {
      if (!this.property) { return; }
      this.$store.dispatch("remove", "property").catch(noop);
    }
  }
});
