// @ts-check

import Vue from "vue";
import BadgeButton from "../BadgeButton.vue";

/**
 * @typedef {any} EndEvent
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "RelationEditValue",
  components: { BadgeButton },
  props: { value: { type: String, default: "" } },
  /** @return {{ current: any, showReset: boolean }} */
  data() {
    return { current: "", showReset: false };
  },
  watch: {
    value() {
      this.current = this.value;
    }
  },
  methods: {
    onEdit() {
      this.showReset = true;
      this.$emit("edit", this.current);
    },
    /**
     * @param {string} value
     */
    setValue(value) {
      this.current = value;
      this.onEdit();
    },
    doReset() {
      this.showReset = false;
      this.$emit("reset");
    }
  }
});
export default component;
