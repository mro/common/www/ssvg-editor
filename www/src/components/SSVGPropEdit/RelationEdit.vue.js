// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import { get, isNil, noop } from "lodash";
import { select } from "d3-selection";
import { isCSS } from "@cern/ssvg-engine";
import RelationCodeEditor from "./RelationCodeEditor.vue";
import Card from "../Card.vue";
import { getAttribute } from "../../utils/element";
import d from "debug";
import RelationEditValue from "./RelationEditValue.vue";
import { BaseKeyboardEventMixin as KBMixin } from "@cern/base-vue";

const debug = d("app:edit");

/**
 * @typedef {{ card: V.Instance<Card>, cm: V.Instance<RelationCodeEditor> }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "RelationEdit",
  components: { Card, RelationCodeEditor, RelationEditValue },
  mixins: [ KBMixin({ local: true }) ],
  props: { title: { type: String, default: "Relation" } },
  /**
   * @return {{
   *  selection: any,
   *  directSelection: any,
   *  value: any,
   *  timer: NodeJS.Timeout|null,
   *  attributeName: string,
   *  querySelector: string,
   *  attributeType: string,
   *  initialValue: any,
   * }}
   */
  data() {
    return {
      selection: null,
      directSelection: null,
      value: null,
      timer: null,
      attributeName: "",
      querySelector: "",
      attributeType: "auto",
      initialValue: undefined
    };
  },
  computed: {
    .../**
      @type {{ normalizedValue(): number, relation(): Element|null }}
    */(mapState("selection", [ "relation", "normalizedValue" ])),
    .../**
      @type {{ directSvg(): any, svg(): any, state(): any, svgSelectors(): any }}
    */(mapState("engine", {
      directSvg: (state) => get(state, [ "directEngine", "svg" ]),
      svg: (state) => get(state, [ "engine", "svg" ]),
      state: (state) => get(state, [ "state" ]),
      svgSelectors: (state) => get(state, [ "svgSelectors" ])
    })),
    .../** @type {{ showKeyHints(): boolean }} */(mapState("ui", [ "showKeyHints" ]))
  },
  watch: {
    relation: {
      immediate: true,
      handler() { this.restore(); this.load(); }
    },
    svg() { this.updateSelection(); },
    state: {
      immediate: true,
      deep: true,
      handler() {
        if (this.timer) {
          clearTimeout(this.timer);
        }
        this.timer = setTimeout(this.onTimer, 150);
      }
    }
  },
  mounted() {
    this.onKey("ctrl-s-keydown", (/** @type {Event} */e) => {
      if (this.$refs.card?.isFocused) {
        e.preventDefault(); this.doApply();
      }
    });
  },
  beforeDestroy() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.restore();
  },
  methods: {
    load() {
      this.attributeName = getAttribute(this.relation, "attribute-name");
      this.querySelector = getAttribute(this.relation, "query-selector");
      this.attributeType = getAttribute(this.relation, "attribute-type") || "auto";
      this.updateSelection();
    },
    updateSelection() {
      this.selection = select(this.svg).selectAll(this.querySelector);
      this.directSelection = select(this.directSvg).selectAll(this.querySelector);
    },
    onTimer() {
      if (!this.$refs.card?.isFocused) {
        const attributeName = this.getAttributeName();
        if (!this.directSelection?.node()) {
          this.value = null;
        }
        else if (isNil(attributeName)) {
          this.value = this.directSelection.text();
        }
        else if ("CSS" === this.getAttributeType()) {
          this.value = this.directSelection.style(attributeName);
        }
        else {
          this.value = this.directSelection.attr(attributeName);
        }
      }
      this.timer = setTimeout(this.onTimer, 500);
    },
    /**
     * @param  {string} value
     */ /* eslint-disable-next-line complexity */
    setValue(value) {
      debug("setValue:%s", value);
      if (!this.selection || !this.directSelection) { return; }

      this.value = value;
      const attributeName = this.getAttributeName();
      if (isNil(attributeName)) {
        if (this.initialValue === undefined && this.selection?.node()) {
          this.initialValue = this.selection.text();
        }
        this.selection.text(value);
        this.directSelection.text(value);
      }
      else if ("CSS" === this.getAttributeType()) {
        if (this.initialValue === undefined && this.selection?.node()) {
          this.initialValue = this.selection.style(attributeName);
        }
        this.selection.style(attributeName, value);
        this.directSelection.style(attributeName, value);
      }
      else {
        if (this.initialValue === undefined && this.selection?.node()) {
          this.initialValue = this.selection.attr(attributeName);
        }
        this.selection.attr(attributeName, value);
        this.directSelection.attr(attributeName, value);
      }
      debug("set initalValue:%s", this.initialValue);
    },
    restore() {
      debug("restore value:%s", this.initialValue);
      if (!this.selection || !this.directSelection) { return; }
      else if (this.initialValue === undefined) { return; }

      const attributeName = this.getAttributeName();
      if (isNil(attributeName)) {
        this.selection.text(this.initialValue);
        this.directSelection.text(this.initialValue);
      }
      else if ("CSS" === this.getAttributeType()) {
        this.selection.style(attributeName, this.initialValue);
        this.directSelection.style(attributeName, this.initialValue);
      }
      else {
        this.selection.attr(attributeName, this.initialValue);
        this.directSelection.attr(attributeName, this.initialValue);
      }
      this.value = this.initialValue;
      this.initialValue = undefined;
    },
    /**
     * @param {string} name
     * @param {string} value
     */
    setParam(name, value) {
      debug("setParam %s=%s", name, value);
      this.restore();
      Vue.set(this, name, value);
      this.updateSelection();
      this.setValue(this.value);
    },
    /**
     * @return {string}
     */
    getAttributeType() {
      if (!this.attributeType || this.attributeType === "auto") {
        return isCSS(this.getAttributeName()) ? "CSS" : "XML";
      }
      else {
        return this.attributeType;
      }
    },
    /**
     * @return {string}
     */
    getAttributeName() {
      return this.attributeName;
    },
    async doApply() {
      if (!this.relation) { return; }
      this.$refs.cm?.doApply();
    },
    doDelete() {
      if (!this.relation) { return; }
      this.$store.dispatch("remove", "relation").catch(noop);
    }
  }
});
