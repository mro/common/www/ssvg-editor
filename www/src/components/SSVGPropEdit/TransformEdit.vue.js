// @ts-check

import Vue from "vue";
import { isEmpty } from "lodash";

import { isCSS } from "@cern/ssvg-engine";
import SVGSelector from "../SVGSelector.vue";

import RelationEdit from "./RelationEdit.vue";
import TransformEditValues from "./TransformEditValues.vue";
import Card from "../Card.vue";
import RelationCodeEditor from "./RelationCodeEditor.vue";


export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "TransformEdit",
  components: { SVGSelector, TransformEditValues, Card, RelationCodeEditor },
  extends: RelationEdit,
  methods: {
    /**
     * @return {string}
     */
    getAttributeName() {
      return isEmpty(this.attributeName) ? "transform" : this.attributeName;
    },
    getAttributeType() {
      if (!this.attributeType || this.attributeType === "auto") {
        /* FIXME: current implementation targets DOM by default */
        if (!this.attributeName || this.attributeName === "transform") {
          return "XML";
        }
        return isCSS(this.getAttributeName()) ? "CSS" : "XML";
      }
      else {
        return this.attributeType;
      }
    }
  }
});
