// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import { get, map, noop } from "lodash";

import { xml } from "@codemirror/lang-xml";
import { Compartment, StateEffect } from "@codemirror/state";
import { foldAll } from "@codemirror/language";
import { closeBrackets, insertCompletionText } from "@codemirror/autocomplete";
import { linter, openLintPanel } from "@codemirror/lint";

import CMColorPlugin from "../../utils/CMColorPlugin";
import { makeSelectorPlugin, selectorTheme } from "../../utils/CMSelectorPlugin";
import CodeMirrorMixin from "../CodeMirrorMixin";
import { bracketConf } from "../CodeMirrorMixinInit";
import { ssvgElements, ssvgLint, ssvgPrettyPrint } from "../../utils/SSVGLint";
import SVGSelector from "../SVGSelector.vue";

/**
 * @typedef {import("@codemirror/autocomplete").Completion} Completion
 * @typedef {import("@codemirror/view").EditorView} EditorView
 *
 * @typedef {{ syntax: Compartment, schema: typeof schema }} Opts
 * @typedef {{ cm: HTMLElement, selector: V.Instance<typeof SVGSelector> }} Refs
 */

const schema = [
  { ...ssvgElements.relation, top: true },
  { ...ssvgElements.transform, top: true },
  { ...ssvgElements.direct, top: true },
  { ...ssvgElements.transition }
];
const languageCompartment = new Compartment();

const component = /** @type {V.Constructor<Opts, Refs>}*/(Vue).extend({
  name: "SSVGCodeEditor",
  components: { SVGSelector },
  mixins: [ CodeMirrorMixin({
    extensions: [
      linter((view) => ssvgLint(view.state?.doc.toString())),
      languageCompartment.of([]),
      CMColorPlugin
    ]
  }) ],
  /** @return {{ isSelecting: boolean }} */
  data() {
    return { isSelecting: false };
  },
  computed: {
    .../** @type {{ relation(): Element|null }} */(mapState("selection", [ "relation" ])),
    .../** @type {{ svgSelectors(): any }} */(mapState("engine", {
      svgSelectors: (state) => get(state, [ "svgSelectors" ])
    })),
    /** @return {string} */
    currentContent() {
      return (this.relation) ? ssvgPrettyPrint(this.relation.outerHTML) : "";
    }
  },
  watch: {
    currentContent(val) { this.setContent(val); }
  },
  mounted() {
    const cm = this.getEditor();
    cm?.dispatch({ effects: bracketConf.reconfigure(closeBrackets()) });
    cm?.dispatch({
      effects: StateEffect.appendConfig.of([
        makeSelectorPlugin(this.getSelector), selectorTheme
      ])
    });

    this.$options.schema = schema;
    this.updateLanguage();
    this.setContent(this.currentContent);
    foldAll(cm);
  },
  methods: {
    updateLanguage() {
      const attributes = [
        {
          name: "query-selector", values: [
            { label: "select", detail: "on document", apply: this.updateSelector, type: "selector", boost: 1 },
            ...map(this.svgSelectors, (s) => ({ label: s, type: "text" }))
          ]
        }
      ];
      this.getEditor().dispatch({ effects: languageCompartment.reconfigure(xml({ elements: this.$options.schema, attributes })) });
    },
    /** @returns {Promise<string>} */
    async getSelector() {
      this.isSelecting = true;
      let selector = "";
      try {
        selector = await this.$refs.selector?.select();
        if (!selector) {
          throw new Error("No selector found");
        }
      }
      catch (e) { console.log("getSelector error", e); }
      this.isSelecting = false;
      return selector;
    },
    /**
     * @param {EditorView} view
     * @param {Completion} completion
     * @param {number} from
     * @param {number} to
     */
    async updateSelector(view, completion, from, to) {
      let selector = await this.getSelector();
      if (selector) {
        selector = `"${selector}"`;
        view.dispatch(insertCompletionText(view.state, selector, from, to));
      }
    },
    async doApply() {
      if (!this.relation) { /* noop */ }
      else if (this.$refs.cm?.getElementsByClassName("cm-lint-marker-error").length > 0) {
        openLintPanel(this.getEditor());
        return false;
      }
      else {
        await this.$store.dispatch("replace", this.editValue).catch(noop);
        return true;
      }
    }
  }
});
export default component;
