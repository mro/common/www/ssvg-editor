// @ts-check

import Vue from "vue";
import { mapState } from "vuex";

import { openLintPanel } from "@codemirror/lint";

import { ssvgElements, ssvgPrettyPrint } from "../../utils/SSVGLint";
import { noop } from "lodash";
import RelationCodeEditor from "./RelationCodeEditor.vue";

/**
 * @typedef {{ schema: typeof schema }} Opts
 * @typedef {{ cm: HTMLElement }} Refs
 */

const schema = [
  { ...ssvgElements.property, top: true },
  { ...ssvgElements.computed, top: true },
  { ...ssvgElements.relation },
  { ...ssvgElements.transform },
  { ...ssvgElements.direct },
  { ...ssvgElements.transition }
];

const component = /** @type {V.Constructor<Opts, Refs>} */ (Vue).extend({
  name: "PropertyCodeEditor",
  extends: RelationCodeEditor,
  computed: {
    .../** @type {{ property(): Element|null }} */(mapState("selection", [ "property" ])),
    /** @return {string} */
    currentContent() {
      return (this.property) ? ssvgPrettyPrint(this.property.outerHTML) : "";
    }
  },
  mounted() {
    this.$options.schema = schema;
    this.updateLanguage();
  },
  methods: {
    async doApply() {
      if (!this.property) { /* noop */ }
      else if (this.$refs.cm?.getElementsByClassName("cm-lint-marker-error").length > 0) {
        openLintPanel(this.getEditor());
        return false;
      }
      else {
        await this.$store.dispatch("replace", this.editValue).catch(noop);
        return true;
      }
    }
  }
});
export default component;
