// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import { noop } from "lodash";
import Card from "../Card.vue";
import { BaseKeyboardEventMixin as KBMixin } from "@cern/base-vue";
import TransitionCodeEditor from "./TransitionCodeEditor.vue";

/** @typedef {{ card: V.Instance<Card>, cm: V.Instance<TransitionCodeEditor> }} Refs */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "TransitionEdit",
  components: { Card, TransitionCodeEditor },
  mixins: [ KBMixin({ local: true }) ],
  computed: {
    .../** @type {{ transition(): Element|null }} */(mapState("selection", [ "transition" ])),
    .../** @type {{ showKeyHints(): boolean }} */(mapState("ui", [ "showKeyHints" ]))
  },
  mounted() {
    this.onKey("ctrl-s-keydown", (/** @type {Event} */e) => {
      if (this.$refs.card?.isFocused) {
        e.preventDefault(); this.doApply();
      }
    });
  },
  methods: {
    async doApply() {
      if (!this.transition) { return; }
      this.$refs.cm?.doApply();
    },
    doDelete() {
      if (!this.transition) { return; }
      this.$store.dispatch("remove", "transition").catch(noop);
    }
  }
});
