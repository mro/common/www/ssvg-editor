// @ts-check

import Vue from "vue";
import { parser } from "@cern/ssvg-engine";
import { findIndex, isEmpty, map } from "lodash";
import { BaseLogger as logger } from "@cern/base-vue";
import d from "debug";
import { transformArgsStr } from "../../utils/ssvg";

const debug = d("app:edit");

/**
 * @typedef {any} EndEvent
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "TransformEditValues",
  props: { value: { type: String, default: "" } },
  /**
   * @return {{
   *   transformList: ssvg.$Transform[]|null, showReset: boolean
   * }}
   */
  data() { return { transformList: null, showReset: false }; },
  watch: {
    value() {
      try {
        this.transformList = parser.parseTTransform(this.value);
      }
      catch (e) {
        logger.error("failed to parse transform: " + e);
      }
    }
  },
  methods: {
    onEdit() {
      if (!this.transformList) { return; }
      this.showReset = true;
      const idx = findIndex(this.transformList,
        (t) => (isEmpty(t.transform) || isEmpty(t.args)));
      if (idx >= 0) {
        debug("invalid transformation: %j", this.transformList[idx]);
      }
      else {
        this.$emit("edit", this.editValue());
      }
    },
    editValue() {
      return map(this.transformList,
        (transform) => `${transform.transform}(${transformArgsStr(transform.args, transform.units)})`).join(" ");
    },
    /**
     * @param {ssvg.$Transform} transform
     * @param {number} index
     * @param {number} value
     */
    setValue(transform, index, value) {
      transform.args[index] = value;
      this.onEdit();
    },
    /**
     * @param {ssvg.$Transform} transform
     * @param {number} index
     * @param {string} value
     */
    setUnit(transform, index, value) {
      transform.units[index] = value;
      this.onEdit();
    },
    /**
     * @param  {number[]} args
     * @param  {(string|null)[]} units
     * @return {string}
     */
    transformArgsStr(args, units) {
      return transformArgsStr(args, units);
    },
    doReset() {
      this.showReset = false;
      this.$emit("reset");
    }
  }
});
export default component;
