// @ts-check
import Vue from "vue";
import { BaseInput } from "@cern/base-vue";

export default /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "Input", /* eslint-disable-line vue/no-reserved-component-names */
  extends: BaseInput,
  props: { name: { type: String, default: "" } }
});
