// @ts-check
import Vue from "vue";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "BadgeButton",
  props: {
    type: { type: String, default: "" },
    badges: { type: Object, default: null },
    isSelected: { type: Boolean, default: false }
  },
  computed: {
    buttonClass() {
      return {
        "btn-outline-dark": !this.isSelected,
        "btn-primary": this.isSelected
      };
    },
    badgeClass() {
      return {
        "text-primary": this.isSelected,
        "border-primary": this.isSelected
      };
    }
  }
});
