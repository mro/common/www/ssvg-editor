// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import { BaseCard, BaseKeyboardEventMixin as kbMixin } from "@cern/base-vue";
import InfoAlert from "./InfoAlert.vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "Card",
  components: { InfoAlert },
  extends: BaseCard,
  mixins: [ kbMixin({ local: true }) ],
  /** @return {{ isSelected: boolean, isFocused: boolean, showInfo: boolean }} */
  data() { return { isSelected: false, isFocused: false, showInfo: false }; },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */(mapState("ui", [ "showKeyHints" ]))
  },
  mounted() {
    this.onKey("ctrl-enter", () => {
      if (this.isFocused) { this.isSelected = true; }
    });
    this.onKey("esc", () => {
      if (this.isFocused) { this.isSelected = false; }
    });
    this.onKey("ctrl-h-keydown", (/** @type {KeyboardEvent} */ e) => {
      if (this.isFocused) {
        e.preventDefault();
        this.showInfo = !this.showInfo;
      }
    });
  },
  methods: {
    toggleSelected() {
      this.isSelected = !this.isSelected;
    }
  }
});
export default component;
