// @ts-check

import Vue from "vue";
import { map } from "lodash";

import BadgeButton from "../BadgeButton.vue";
import RelationMixin from "./RelationMixin";
import { getAttribute } from "../../utils/element";
import { getTransformValues } from "../../utils/ssvg";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "Transform",
  components: { BadgeButton },
  mixins: [ RelationMixin ],
  computed: {
    /** @returns {string} */
    transformList() {
      return map(getTransformValues(this.relation), "transform").join(", ") || "...";
    },
    /** @returns {{ selector: string, transform: string }} */
    badges() {
      return {
        selector: getAttribute(this.relation, "query-selector"),
        transform: this.transformList
      };
    }
  }
});
