// @ts-check

import Vue from "vue";
import PropertyMixin from "./PropertyMixin";
import BadgeButton from "../BadgeButton.vue";
import { getAttribute } from "../../utils/element";

/**
 * @typedef {{ value: HTMLInputElement }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "PropertyNumber",
  components: { BadgeButton },
  mixins: [ PropertyMixin ],
  /** @return {{ isFocused: boolean }} */
  data() { return { isFocused: false }; },
  computed: {
    min() { return getAttribute(this.property, "min"); },
    max() { return getAttribute(this.property, "max"); }
  },
  watch: {
    value() { this.$refs.value.value = this.value; }
  },
  mounted() {
    this.$refs.value.value = this.value;
  },
  methods: {
    /** @param  {Event} event */
    onInputClick(event) {
      event.stopPropagation();
    },
    onInputChange() {
      this.$store.commit("engine/updateState",
        { [this.name]: this.$refs.value.value });
    },
    doCreateRelation() {

    }
  }
});
