// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import { BaseDialog } from "@cern/base-vue";

import { genId } from "../../utils";
import { select } from "d3-selection";
import { color } from "d3-color";
import Input from "../Input.vue";
import { get } from "lodash";
import d from "debug";

const debug = d("app:edit");

/**
 * @typedef {{ name: string, isAvailable?: (selection: ReturnType<select<Element>>|null) => boolean }} RelationType
 * @typedef {{
 *  "dialog": V.Instance<BaseDialog>,
 *  "from": V.Instance<Input>,
 *  "to": V.Instance<Input>,
 *  "type": V.Instance<Input>,
 *  "attribute-name": V.Instance<Input>
 * }} Refs
 */

const hasWidthHeight = new Set([ "filter", "foreignObject", "image", "pattern",
                                 "rect", "svg", "use", "mask" ]);

/** @type {{ [name: string]: RelationType }} */
const types = {
  custom: { name: "custom" },
  color: { name: "color" },
  opacity: { name: "opacity" },
  width: { name: "width", isAvailable(selection) {
    return hasWidthHeight.has(/** @type {Element}*/(selection?.node())?.tagName);
  } },
  height: { name: "height", isAvailable(selection) {
    return hasWidthHeight.has(/** @type {Element}*/(selection?.node())?.tagName);
  } }
};

const options = {
  types
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: "CreateRelationDialog",
  components: { BaseDialog, Input }, /* eslint-disable-line vue/no-reserved-component-names -- Input */
  props: { query: { type: String, default: null } },
  ...options,
  /**
   * @return {{
   * type: keyof types, svgAttributesId: string, isValid: boolean,
   * selection: ReturnType<select<Element>>|null }}
   */
  data() { return { type: "color", svgAttributesId: genId(), isValid: true, selection: null }; },
  computed: {
    .../** @return {{ svg(): Element|null }} */mapState("engine", {
      svg(/** @type {AppStore.SSVGEngine} */state) { return get(state, "directEngine.svg"); } })
  },
  watch: {
    svg() { this.$nextTick(this.onUpdate); },
    type() { this.$nextTick(this.onUpdate); },
    query() { this.$nextTick(this.onUpdate); }
  },
  methods: {
    async request() {
      if (!(await this.$refs.dialog.request())) { return null; }
      return this.createRelation();
    },
    async checkValidity() { /* eslint-disable-line complexity */
      if (!this.query) { return; }
      let valid = true;

      for (const ref of [ "attribute-name", "from", "to" ]) {
        valid = await (/** @type {V.Instance<Input>} */(
          this.$refs?.[ref])?.checkValidity() ?? true) && valid;
      }

      if (this.type === "color") {
        if ((!this.selection?.node() || !this.selection?.style("fill")) && !this.$refs["from"]?.editValue) {
          this.$refs["from"]?.addError("x-error", "Failed to retrieve \"from\" value");
          valid = false;
        }
        else {
          this.$refs["from"]?.removeError("x-error");
        }
      }
      this.isValid = valid;
    },
    onUpdate() { /* eslint-disable-line complexity */
      if (!this.svg || !this.query) { return; }
      this.selection = select(this.svg).selectAll(this.query);
      if (!this.selection.node()) {
        this.$refs["type"]?.addWarning("x-warn", "failed to select nodes");
      }
      else if (this.type === "custom") {
        this.$refs["type"]?.addWarning("x-warn", '"custom" is an expert type, please select another type for guided creation');
      }
      else {
        this.$refs["type"]?.removeWarning("x-warn");
      }

      if (this.type === "color") {
        this.$refs["from"].editValue =
          color(this.getCurrentValue("fill", "#000000"))?.formatHex() ?? null;
      }
      else if (this.type === "opacity") {
        const from = this.getCurrentValue("opacity", "1");
        this.$refs["from"].editValue = from;
        this.$refs["to"].editValue = ((Number(from) >= 0.5) ? "0" : "1");
      }
      else if (this.type === "width" || this.type === "height") {

        const from =
          /** @type {SVGGraphicsElement} */(this.selection?.node())?.getBBox()?.[this.type] ?? 100;
        this.$refs["from"].editValue = from.toString();
        this.$refs["to"].editValue = `${(from * 2)}`;
      }
      this.checkValidity();
    },
    /**
     * @param {Element} element
     * @param {string} name
     * @param {boolean} optional
     */
    addAttribute(element, name, optional = false) {
      const value = /** @type {V.Instance<Input>} */(this.$refs[name])?.editValue;
      if (!optional || value) {
        element.setAttribute(name, value || "");
      }
      return element;
    },
    createRelation() { /* eslint-disable-line complexity */
      const element = document.createElementNS("http://www.w3.org/2000/svg", "relation");
      element.setAttribute("query-selector", this.query);
      if (this.type === "custom") {
        this.addAttribute(element, "attribute-name", true);
        this.addAttribute(element, "attribute-type", true);
        this.addAttribute(element, "calc-mode", true);
        this.addAttribute(element, "from", true);
        this.addAttribute(element, "to");
      }
      else if (this.type === "color") {
        this.addAttribute(element, "from");
        this.addAttribute(element, "to");
        element.setAttribute("attribute-name", "fill");
        element.setAttribute("attribute-type", "CSS");
      }
      else if (this.type === "opacity") {
        element.setAttribute("from", this.$refs["from"]?.editValue ||
          ((Number(this.$refs["to"]?.editValue) >= 0.5) ? "0" : "1"));
        this.addAttribute(element, "to");
        element.setAttribute("attribute-name", "opacity");
        element.setAttribute("attribute-type", "CSS");
      }
      else if (this.type === "width" || this.type === "height") {
        element.setAttribute("from", this.$refs["from"]?.editValue ||
          this.getCurrentValue(this.type, "0"));
        this.addAttribute(element, "to");
        element.setAttribute("attribute-name", this.type);
        element.setAttribute("attribute-type", "CSS");
      }
      debug("new relation:", element);
      return element;
    },
    /**
     * @template T
     * @param {string} style style attribute for lookup
     * @param {T} defaultValue fallback value
     * @return {T|string}
     */
    getCurrentValue(style, defaultValue) {
      if (this.selection?.node()) {
        return this.selection.style(style) || defaultValue;
      }
      return defaultValue;
    }
  }
});
export default component;
