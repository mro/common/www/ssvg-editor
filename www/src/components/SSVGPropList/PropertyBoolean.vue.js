// @ts-check

import Vue from "vue";
import PropertyMixin from "./PropertyMixin";
import BadgeButton from "../BadgeButton.vue";
import { getAttribute } from "../../utils/element";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "PropertyBoolean",
  components: { BadgeButton },
  mixins: [ PropertyMixin ],
  computed: {
    /** @returns {string} */
    min() { return getAttribute(this.property, "min"); },
    /** @returns {string} */
    max() { return getAttribute(this.property, "max"); }
  },
  methods: {
    /** @param  {Event} event */
    onInputClick(event) {
      event.stopPropagation();
      this.$store.commit("engine/updateState", { [this.name]: !this.value });
    }
  }
});
