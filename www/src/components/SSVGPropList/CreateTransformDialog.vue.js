// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import draggable from "vuedraggable";
import { parser } from "@cern/ssvg-engine";
import { select } from "d3-selection";
import d from "debug";
import { cloneDeep, fill, forEach, get, toString } from "lodash";
import { BaseKeyboardEventMixin as KBMixin } from "@cern/base-vue";

import { genId } from "../../utils";
import { transformArgsStr } from "../../utils/ssvg";
import { checkTransformRange, transformList } from "../../utils/SSVGLint";
import Input from "../Input.vue";
import ErrorList from "../ErrorList.vue";
import InfoAlert from "../InfoAlert.vue";

const debug = d("app:edit");

/**
 * @typedef {{ custom?: boolean }} TransformOpts
 * @typedef {import("d3-selection").BaseType} BaseType
 * @typedef {import("d3-selection").Selection<BaseType, unknown, Element, any>} Selection
 */

/**
 * @typedef {typeof import("@cern/base-vue").BaseDialog} BaseDialog
 * @typedef {{
 *  dialog: V.Instance<BaseDialog>,
 *  from: V.Instance<Input>[],
 *  to: V.Instance<Input>[],
 *  errList: V.Instance<ErrorList>
 * }} Refs
 * @typedef {{ transformList: string[] }} Opts
 */

const component = /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "CreateTransformDialog",
  /* eslint-disable-next-line vue/no-reserved-component-names -- Input */
  components: { Input, ErrorList, draggable, InfoAlert },
  mixins: [ KBMixin({ local: true }) ],
  props: { query: { type: String, default: null } },
  ...{ transformList },
  /**
   * @return {{
   * svgAttributesId: string, isValid: boolean,
   * selection: Selection|null,
   * reqId: number,
   * transforms: (ssvg.$TransformRange & TransformOpts)[],
   * showInfo: boolean, isFocused: boolean }}
   */
  data() {
    return {
      svgAttributesId: genId(),
      isValid: true,
      selection: null,
      reqId: 0,
      transforms: [],
      showInfo: false,
      isFocused: false
    };
  },
  computed: {
    .../** @type {{ svg(): Element|null }} */(mapState("engine", { svg(state) { return get(state, "directEngine.svg"); } })),
    .../** @type {{ showKeyHints(): boolean }} */(mapState("ui", [ "showKeyHints" ]))
  },
  watch: {
    svg() { this.$nextTick(this.onUpdate); },
    query() { this.$nextTick(this.onUpdate); }
  },
  mounted() {
    this.onKey("ctrl-h-keydown", (/** @type {Event} */ e) => {
      if (this.isFocused) {
        e.stopImmediatePropagation();
        e.preventDefault();
        this.showInfo = !this.showInfo;
      }
    });
  },
  methods: {
    async request() {
      this.reqId += 1;
      this.loadTransform();
      if (!(await this.$refs.dialog.request())) { return null; }
      return this.createTransform();
    },
    async checkValidity() { /* eslint-disable-line complexity */
      if (!this.query) { return; }
      let valid = true;

      for (const w of this.$refs["from"] ?? []) {
        valid = await (w?.checkValidity() ?? true) && valid;
      }
      for (const w of this.$refs["to"] ?? []) {
        valid = await (w?.checkValidity() ?? true) && valid;
      }

      if (!this.transforms) {
        this.$refs["errList"]?.addError("x-empty", "please add a transform");
        valid = false;
      }
      else {
        this.$refs["errList"]?.removeError("x-empty");
      }

      this.isValid = valid;
    },
    loadTransform() {
      const current = this.getCurrentValue();
      if (current) {
        this.transforms = parser.normalizeTransform([ current, [] ]);
      }
      else {
        this.addTransform();
      }
    },
    onUpdate() {
      if (!this.svg || !this.query) { return; }
      this.selection = select(this.svg).selectAll(this.query);
      if (!this.selection?.node()) {
        this.$refs["errList"]?.addWarning("x-warn", "failed to select nodes");
      }
      this.checkValidity();
    },
    /**
     * @param {Element} element
     * @param {string} name
     * @param {string | null} defaultValue
     */
    addAttribute(element, name, defaultValue = null) {
      const value = /** @type {V.Instance<Input>} */(this.$refs[name])?.editValue;
      if (value || defaultValue !== null) {
        element.setAttribute(name, value ?? defaultValue ?? "");
      }
      return element;
    },
    createTransform() {
      const element = document.createElementNS("http://www.w3.org/2000/svg", "transform");
      element.setAttribute("query-selector", this.query);
      element.setAttribute("attribute-name", "transform");
      this.addAttribute(element, "attribute-type", "CSS");
      this.addAttribute(element, "calc-mode");
      element.setAttribute("from", this.genTransformStep(0));
      element.setAttribute("to", this.genTransformStep(1));
      return element;
    },
    /**
     * @return {ssvg.$Transform[]|null}
     */
    getCurrentValue() {
      if (!this.selection?.node()) { return null; }

      try {
        return parser.parseTTransform(
          this.selection.style("transform") ?? this.selection.attr("transform"));
      }
      catch (err) {
        console.warn("Failed to parse transform: " + err);
      }
      return null;
    },
    /**
     * @param  {number[]} args
     * @param  {(string|null)[]} units
     * @return {string}
     */
    transformArgsStr(args, units) {
      return transformArgsStr(args, units);
    },
    /**
     * @param {number} transformIndex
     * @param {number} argIndex
     * @param {string} value
     */
    updateArgs(transformIndex, argIndex, value) {
      const transform = cloneDeep(this.transforms[transformIndex]);
      /** @type {V.Instance<Input>} */
      const argWidget = get(this.$refs, [ argIndex ? "to" : "from", transformIndex ]);

      if (!value) { // widget reset
        argWidget?.removeError("x-parse");
        argWidget?.removeWarning("x-parse");
        this.checkValidity();
        return;
      }

      try {
        const args = parser.parseTTransform(`${transform.transform}(${value})`)?.[0];
        if (!args) { throw new Error("unknown error"); }

        transform.args[argIndex] = args.args;
        transform.units = args.units;
        forEach(transform.args, (arg) => {
          const len = arg.length;
          arg.length = args.args.length;
          fill(arg, 0, len, args.args.length);
        });
        try {
          if (!transform.custom) {
            checkTransformRange(transform);
          }
          argWidget?.removeWarning("x-parse");
        }
        catch (err) {
          argWidget?.addWarning("x-parse", toString(err));
        }
        argWidget?.removeError("x-parse");
        this.checkValidity();
        Vue.set(this.transforms, transformIndex, transform);
      }
      catch (err) {
        argWidget?.removeWarning("x-parse");
        argWidget?.addError("x-parse", "Failed to parse transform: " + err);
        this.isValid = false;
        debug("Failed to parse transform: ", err);
      }
    },
    /**
     * @param {number} transformIndex
     * @param {string} value
     */
    updateTransform(transformIndex, value) {
      const transform = cloneDeep(this.transforms[transformIndex]);

      if (value === "custom") {
        transform.custom = true;
        this.transforms[transformIndex] = transform;
        this.updateArgs(transformIndex, 0, "");
        this.updateArgs(transformIndex, 1, "");
        return;
      }
      else {
        transform.transform = value;
        this.transforms[transformIndex] = transform;
        this.updateArgs(transformIndex, 0,
          this.transformArgsStr(transform.args[0], transform.units));
      }
    },
    /**
     * @param {number} transformIndex
     */
    removeAt(transformIndex) {
      const transforms = cloneDeep(this.transforms);
      transforms?.splice(transformIndex, 1);
      this.transforms = transforms;
    },
    addTransform() {
      const transforms = cloneDeep(this.transforms);
      transforms.push({
        transform: "translate",
        args: [ [ 0, 0 ], [ 100, 100 ] ],
        units: [ "px", "px" ]
      });
      this.transforms = transforms;
    },
    /**
     * @param {number} step
     */
    genTransformStep(step) {
      return this.transforms.map(
        (tr) => `${tr.transform}(${tr.units.map((u, idx) => (tr.args[step][idx] + (u ?? ""))).join(", ")})`
      ).join(", ");
    }
  }
});
export default component;
