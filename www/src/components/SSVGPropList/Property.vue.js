// @ts-check

import Vue from "vue";
import { forEach, get, invoke, isEmpty } from "lodash";

import PropertyNumber from "./PropertyNumber.vue";
import PropertyBoolean from "./PropertyBoolean.vue";
import PropertyEnumerated from "./PropertyEnumerated.vue";
import PropertyAuto from "./PropertyAuto.vue";
import d from "debug";

const debug = d("app:proplist");

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "Property",
  components: { PropertyNumber, PropertyBoolean, PropertyEnumerated, PropertyAuto },
  props: {
    /* eslint-disable-next-line vue/require-prop-types */
    state: { /* type: SVGElement, VueX typecheck issues */ default: null },
    /* eslint-disable-next-line vue/require-prop-types */
    property: { /* type: SVGElement, VueX typecheck issues */ default: null }
  },
  /**
   * @return {{
   *  relations: { [index: string]: Element }|null,
   *  transforms: { [index: string]: Element }|null
   * }}
   */
  data() {
    return { relations: null, transforms: null };
  },
  computed: {
    /** @return {string} */
    type() { return invoke(this.property, "getAttribute", "type"); }
  },
  watch: {
    property() { this.load(); }
  },
  mounted() {
    this.load();
  },
  methods: {
    load() {
      /** @type {{ [index: string]: Element }} */
      const relations = {};
      /** @type {{ [index: string]: Element }} */
      const transforms = {};

      forEach(get(this, [ "property", "children" ]), (elt, idx) => {
        if (elt.tagName === "relation") {
          relations[idx] = elt;
        }
        else if (elt.tagName === "transform") {
          transforms[idx] = elt;
        }
        else {
          debug("unknown property child element: %s", elt.tagName);
        }
      });
      this.relations = isEmpty(relations) ? null : relations;
      this.transforms = isEmpty(transforms) ? null : transforms;
    }
  }
});
