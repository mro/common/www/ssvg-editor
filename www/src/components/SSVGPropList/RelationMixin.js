// @ts-check

import { mapGetters, mapState } from "vuex";
import Transition from "./Transition.vue";
import { mixinMaker } from "../../utils";


export default mixinMaker({
  name: "Relation",
  components: { Transition },
  props: {
    state: { type: SVGElement, default: null },
    property: { type: SVGElement, default: null },
    relation: { type: SVGElement, default: null }
  },
  /**
   *
   * @return {{ transitions: Element[] | null }}
   */
  data() {
    return { transitions: null };
  },
  computed: {
    .../** @type {{ selectedElement(): Element|null }} */(
      mapGetters("selection", [ "selectedElement" ])),
    .../** @type {{ selectedRelation(): Element|null }} */(
      mapState("selection", { selectedRelation: "relation" })),
    /** @return {boolean} */
    isSelected() {
      return this.relation === this.selectedElement;
    },
    /** @returns {boolean} */
    isInSelection() {
      return this.relation === this.selectedRelation;
    }
  },
  watch: {
    relation() { this.load(); }
  },
  mounted() {
    this.load();
  },
  methods: {
    load() {
      /** @type {HTMLCollectionOf<Element>|undefined} */
      const transitions =
        /** @type {SVGElement|null} */(this.relation)?.getElementsByTagName("transition");
      this.transitions = transitions?.length ? Array.from(transitions) : null;
    },
    async setSelected() {
      if (!this.isSelected) {
        await this.$store.dispatch("selection/select", {
          state: this.state, property: this.property, relation: this.relation
        });
      }
    },
    async doCreateTransition() {
      try {
        await this.setSelected();
        const prop = await this.$root.$refs?.createTransitionDialog?.request();
        if (!prop) { return; }
        await this.$store.dispatch("add", prop);
      }
      catch (e) {
        console.warn(e);
      }
    }
  }
});
