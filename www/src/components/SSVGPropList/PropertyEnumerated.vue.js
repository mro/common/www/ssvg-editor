// @ts-check

import Vue from "vue";
import PropertyMixin from "./PropertyMixin";
import BadgeButton from "../BadgeButton.vue";
import { getAttribute } from "../../utils/element";
import { parser } from "@cern/ssvg-engine";


/**
 * @typedef {typeof import("@cern/base-vue").BaseSelect} BaseSelect
 * @typedef {{ value: V.Instance<BaseSelect> }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "PropertyEnumerated",
  components: { BadgeButton },
  mixins: [ PropertyMixin ],
  computed: {
    /** @return {string[]} */
    valueList() {
      try {
        return parser.parseList({ index: 0, value: getAttribute(this.property, "values") },
          parser.parseString);
      }
      catch {
        return [];
      }
    }
  },
  watch: {
    value() { this.$refs.value.editValue = this.value; }
  },
  mounted() {
    this.$refs.value.editValue = this.value;
  },
  methods: {
    onEdit() {
      if (!this.$refs.value.hasFocus) { return; }
      this.$store.commit("engine/updateState", { [this.name]: this.$refs?.value?.editValue });
    }
  }
});
