// @ts-check

import { noop } from "lodash";
import Vue from "vue";
import { mapGetters } from "vuex";

import { getAttribute } from "../../utils/element";
import BadgeButton from "../BadgeButton.vue";


const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "Transition",
  components: { BadgeButton },
  props: {
    /* eslint-disable-next-line vue/require-prop-types */
    state: { /* type: SVGElement, VueX typecheck issues */ default: null },
    /* eslint-disable-next-line vue/require-prop-types */
    property: { /* type: SVGElement, VueX typecheck issues */ default: null },
    /* eslint-disable-next-line vue/require-prop-types */
    relation: { /* type: SVGElement, VueX typecheck issues */ default: null },
    /* eslint-disable-next-line vue/require-prop-types */
    transition: { /* type: SVGElement, VueX typecheck issues */ default: null }
  },
  computed: {
    .../**
    @type {{ selectedElement(): Element|null }} */(
      mapGetters("selection", [ "selectedElement" ])),
    /** @return {boolean} */
    isSelected() {
      return this.transition === this.selectedElement;
    },
    /** @returns {string} */
    duration() {
      return getAttribute(this.transition, "duration") ?? "0s";
    },
    /** @returns {string} */
    tfunc() {
      return getAttribute(this.transition, "timing-function") ?? "linear";
    },
    /** @returns {string} */
    type() {
      return getAttribute(this.transition, "type") ?? "strict";
    },
    /** @returns {string} */
    info() {
      if (!this.transition) { return ""; }
      return `${this.duration} ${this.tfunc} ${this.type}`;
    }
  },
  methods: {
    setSelected() {
      this.$store.dispatch("selection/select", {
        state: this.state, relation: this.relation, property: this.property,
        transition: this.transition
      }).catch(noop);
    }
  }
});
export default component;
