// @ts-check

import Vue from "vue";
import Relation from "./Relation.vue";
import Transform from "./Transform.vue";
import Direct from "./Direct.vue";
import CreateRelationButton from "./CreateRelationButton.vue";
import { forEach, get, isEmpty } from "lodash";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "RelationList",
  components: { Relation, Transform, Direct, CreateRelationButton },
  props: {
    /* eslint-disable-next-line vue/require-prop-types */
    state: { /* type: SVGElement, VueX typecheck issues */ default: null },
    /* eslint-disable-next-line vue/require-prop-types */
    property: { /* type: SVGElement, VueX typecheck issues */ default: null }
  },
  /**
   * @return {{
   *  relations: { [index: string]: Element }|null,
   *  transforms: { [index: string]: Element }|null
   *  directs: { [index: string]: Element }|null
   * }}
   */
  data() {
    return { relations: null, transforms: null, directs: null };
  },
  watch: {
    property() { this.load(); }
  },
  mounted() {
    this.load();
  },
  methods: {
    load() {
      /** @type {{ [index: string]: Element }} */
      const relations = {};
      /** @type {{ [index: string]: Element }} */
      const transforms = {};
      /** @type {{ [index: string]: Element }} */
      const directs = {};

      forEach(get(this, [ "property", "children" ]), (elt, idx) => {
        if (elt.tagName === "relation") {
          relations[idx] = elt;
        }
        else if (elt.tagName === "transform") {
          transforms[idx] = elt;
        }
        else if (elt.tagName === "direct") {
          directs[idx] = elt;
        }
      });
      this.relations = isEmpty(relations) ? null : relations;
      this.transforms = isEmpty(transforms) ? null : transforms;
      this.directs = isEmpty(directs) ? null : directs;
    }
  }
});
