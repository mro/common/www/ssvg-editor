// @ts-check

import Vue from "vue";

import Input from "../Input.vue";
import ErrorList from "../ErrorList.vue";
import { genId } from "../../utils";

/**
 * @typedef {{ x: number|null, y: number|null, z: number|null }} Position
 * @typedef {{ x: string|null, y: string|null, z: string|null }} Units
 *
 * @typedef {typeof import("@cern/base-vue").BaseInput} BaseInput
 * @typedef {{errors: V.Instance<BaseInput>}} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "CreateTransformTranslate",
  /* eslint-disable-next-line vue/no-reserved-component-names -- Input */
  components: { Input, ErrorList },
  props: {
    selection: { type: Object, default: null }
  },
  /**
   * @return {{
   * isValid: boolean,
   * from: Position,
   * to: Position
   * units: Units,
   * unitsListId: string
   * }}
   */
  data() {
    return {
      isValid: true,
      from: { x: 0, y: 0, z: 0 }, to: { x: 0, y: 0, z: 0 },
      units: { x: null, y: null, z: null },
      unitsListId: genId()
    };
  },
  watch: {
    selection() { this.$nextTick(this.onUpdate); },
    isValid() { this.$emit("is-valid", this.isValid); }
  },
  methods: {
    /** @returns {Promise<void>} */
    async checkValidity() {
      const valid = true;
      this.$refs["errors"]?.removeWarning("x-transform-set");
      if (this.selection?.style("transform")) {
        this.$refs["errors"]?.addWarning("x-transform-set",
          "There is already a CSS transform on this element, it may conflict");
      }
      this.isValid = valid;
    },
    onUpdate() {
      this.from = { x: 0, y: 0, z: 0 };
      this.to = { x: 0, y: 0, z: 0 };
      this.units = { x: null, y: null, z: null };

      this.checkValidity();
    },
    /**
     * @param {"x" | "y" | "z"} axis
     */
    isEnabled(axis) {
      if (!this.to[axis]) {
        return false;
      }
      else if (this.to[axis] === 0) {
        return this.from[axis] && this.from[axis] !== 0;
      }
      return true;
    },
    /** @param {SVGElement} element */
    makeRelation(element) { /* eslint-disable-line complexity */
      element.setAttribute("attribute-name", "transform");
      element.setAttribute("attribute-type", "CSS");
      if (this.isEnabled("z")) {
        element.setAttribute("from", `translate3d(${this.from.x || 0}, ${this.from.y || 0}, ${this.from.z || 0})`);
        element.setAttribute("to", `translate3d(${this.to.x || 0}, ${this.to.y || 0}, ${this.to.z || 0})`);
      }
      else {
        element.setAttribute("from", `translate(${this.from.x || 0}, ${this.from.y || 0})`);
        element.setAttribute("to", `translate(${this.to.x || 0}, ${this.to.y || 0})`);
      }
    }
  }
});
export default component;
