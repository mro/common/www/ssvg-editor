// @ts-check

import Vue from "vue";
import { every } from "lodash";
import PropertyMixin from "./PropertyMixin";
import BadgeButton from "../BadgeButton.vue";
import { BaseInput } from "@cern/base-vue";

/**
 * @typedef {{ value: V.Instance<BaseInput> }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "PropertyAuto",
  components: { BaseInput, BadgeButton },
  mixins: [ PropertyMixin ],
  watch: {
    value() {
      if (!this.$refs.value.hasFocus) {
        this.$refs.value.editValue = JSON.stringify(this.value);
      }
    }
  },
  mounted() {
    this.$refs.value.editValue = this.value;
  },
  methods: {
    onEdit() {
      if (!this.$refs.value.hasFocus) { return; }
      /** @type {string|number|null} */
      let value = this.$refs?.value?.editValue;
      try {
        /*
            special case, empty string and several zeroes are considered as zero
            this just makes the input a bit more convenient
        */
        if (every(value, (v) => (v === "0"))) {
          value = 0;
        }
        else {
          value = JSON.parse(/** @type {string} */(value));
        }
      }
      catch { /* noop */ }
      this.$store.commit("engine/updateState", { [this.name]: value });
    }
  }
});
