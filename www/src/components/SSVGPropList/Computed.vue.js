// @ts-check
import Vue from "vue";
import PropertyMixin from "./PropertyMixin";
import BadgeButton from "../BadgeButton.vue";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "Computed",
  components: { BadgeButton },
  mixins: [ PropertyMixin ]
});
