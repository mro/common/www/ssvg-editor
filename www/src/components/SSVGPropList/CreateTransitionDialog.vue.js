// @ts-check

import Vue from "vue";
import { assign } from "lodash";

/**
 * @typedef {typeof import("@cern/base-vue").BaseDialog} BaseDialog
 * @typedef {{ dialog: V.Instance<BaseDialog> }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "CreateTransitionDialog",
  data() {
    return {
      durationValue: ".25", durationUnit: "s", tfunc: "linear", type: "strict",
      delayValue: "", delayUnit: "s"
    };
  },
  methods: {
    async request() {
      // @ts-ignore: `data()` exists in options
      assign(this.$data, this.$options.data()); // used to reset component
      if (!(await this.$refs.dialog.request())) { return null; }

      const ret = document.createElementNS("http://www.w3.org/2000/svg",
        "transition");
      ret.setAttribute("duration", this.durationValue ? `${this.durationValue}${this.durationUnit}` : "0s");
      ret.setAttribute("type", this.type);
      ret.setAttribute("timing-function", this.tfunc);
      if (this.delayValue) {
        ret.setAttribute("delay", `${this.delayValue}${this.delayUnit}`);
      }
      return ret;
    }
  }
});
