// @ts-check

import Vue from "vue";
import { last, noop } from "lodash";
import SVGSelector from "../SVGSelector.vue";
import CreateRelationDialog from "./CreateRelationDialog.vue";
import CreateTransformDialog from "./CreateTransformDialog.vue";
import CreateDirectDialog from "./CreateDirectDialog.vue";

/**
 * @typedef {typeof import("@cern/base-vue").BaseDialog} BaseDialog
 * @typedef {{
 *  selector: V.Instance<typeof SVGSelector>,
 *  relationDialog: V.Instance<BaseDialog>,
 *  transformDialog: V.Instance<BaseDialog>,
 *  directDialog: V.Instance<BaseDialog>
 * }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "CreateRelationButton",
  components: { SVGSelector, CreateRelationDialog, CreateTransformDialog,
    CreateDirectDialog },
  props: {
    state: { type: SVGElement, default: null },
    property: { type: SVGElement, default: null }
  },
  /**
   * @return {{ isSelecting: boolean, query: string|null }}
   */
  data() { return { isSelecting: false, query: null }; },
  methods: {
    /**
     * @param {string} type
     */
    async add(type) {
      try {
        if (!this.property) { return; }
        this.isSelecting = true;
        this.query = await this.$refs.selector.select();
        this.isSelecting = false;
        if (!this.query) { return; }

        let element;
        if (type === "relation") {
          element = await this.$refs.relationDialog.request();
        }
        else if (type === "transform") {
          element = await this.$refs.transformDialog.request();
        }
        else if (type === "direct") {
          element = await this.$refs.directDialog.request();
        }
        if (!element) { return; }

        await this.$store.dispatch("add", element).catch(noop);

        const relation = last(this.property.getElementsByTagName(type));
        await this.$store.dispatch("selection/select", {
          state: this.state, property: this.property, relation
        }).catch(noop);
        this.query = null;
      }
      catch (e) {
        console.warn(e);
      }
      finally {
        this.isSelecting = false;
      }
    }
  }
});
