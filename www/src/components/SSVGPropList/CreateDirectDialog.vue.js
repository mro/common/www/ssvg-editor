// @ts-check

import Vue from "vue";
import { mapState } from "vuex";

import { genId } from "../../utils";
import { select } from "d3-selection";
import Input from "../Input.vue";
import { get } from "lodash";
import d from "debug";
import InfoAlert from "../InfoAlert.vue";
import ErrorList from "../ErrorList.vue";

const debug = d("app:edit");

/**
 * @typedef {typeof import("@cern/base-vue").BaseDialog} BaseDialog
 * @typedef {{ name: string, isAvailable?: (selection: ReturnType<select<Element>>|null) => boolean }} RelationType
 * @typedef {{
 *  dialog: V.Instance<BaseDialog>,
 *  errList: V.Instance<ErrorList>
 *  }} Refs
 */


const component = /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "CreateDirectDialog",
  /* eslint-disable-next-line vue/no-reserved-component-names -- Input */
  components: { ErrorList, Input, InfoAlert },
  props: { query: { type: String, default: null } },
  /**
   * @return {{
   * svgAttributesId: string, isValid: boolean,
   * selection: ReturnType<select<Element>>|null
   * showInfo: boolean }}
   */
  data() { return { svgAttributesId: genId(), isValid: true, selection: null, showInfo: false }; },
  computed: {
    .../** @return {{ svg(): Element|null }} */mapState("engine", {
      svg(/** @type {AppStore.SSVGEngine} */state) { return get(state, "directEngine.svg"); } })
  },
  watch: {
    svg() { this.$nextTick(this.onUpdate); },
    query() { this.$nextTick(this.onUpdate); }
  },
  methods: {
    async request() {
      if (!(await this.$refs.dialog?.request())) { return null; }
      return this.createRelation();
    },
    async checkValidity() {
      if (!this.query) { return; }
      this.isValid = true;
    },
    onUpdate() {
      if (!this.svg || !this.query) { return; }
      this.selection = select(this.svg).selectAll(this.query);
      if (!this.selection.node()) {
        this.$refs["errList"]?.addWarning("x-warn", "failed to select nodes");
      }

      this.checkValidity();
    },
    /**
     * @param {Element} element
     * @param {string} name
     * @param {boolean} optional
     */
    addAttribute(element, name, optional = false) {
      const value = /** @type {V.Instance<Input>} */(this.$refs[name])?.editValue;
      if (!optional || value) {
        element.setAttribute(name, value || "");
      }
      return element;
    },
    createRelation() {
      const element = document.createElementNS("http://www.w3.org/2000/svg", "direct");
      element.setAttribute("query-selector", this.query);

      this.addAttribute(element, "attribute-name", true);
      this.addAttribute(element, "attribute-type", true);
      this.addAttribute(element, "calc-mode", true);
      this.addAttribute(element, "onupdate");

      debug("new direct:", element);
      return element;
    }
  }
});
export default component;
