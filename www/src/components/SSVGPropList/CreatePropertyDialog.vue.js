// @ts-check

import Vue from "vue";
import { assign, isNaN, map, toNumber, toString } from "lodash";
import { tvalueToString } from "../../utils/ssvg";
import PropertyEnumValuesEdit from "../SSVGPropEdit/PropertyEnumValuesEdit.vue";

/**
 * @typedef {typeof import("@cern/base-vue").BaseToggle} BaseToggle
 * @typedef {typeof import("@cern/base-vue").BaseDialog} BaseDialog
 * @typedef {{
 *  initial: V.Instance<BaseToggle>
 *  dialog: V.Instance<BaseDialog>
 * }} Refs
 */

export default /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "CreatePropertyDialog",
  components: { PropertyEnumValuesEdit },
  data() {
    return {
      name: "", type: "number", computed: false,
      min: 0, max: 1, enumValues: []
    };
  },
  methods: {
    async request() {
      // @ts-ignore: `data()` exists in options
      assign(this.$data, this.$options.data()); // used to reset component
      if (!(await this.$refs.dialog.request())) { return null; }

      const ret = document.createElementNS("http://www.w3.org/2000/svg",
        this.computed ? "computed" : "property");
      ret.setAttribute("name", this.name);
      ret.setAttribute("type", this.type);

      if (this.type === "number") {
        return this._createNumber(ret);
      }
      else if (this.type === "enum") {
        return this._createEnum(ret);
      }
      else if (this.type === "boolean") {
        return this._createBoolean(ret);
      }
      else if (this.type === "auto") {
        return this._createAuto(ret);
      }
      return null;
    },
    /** @param {Element} ret */
    _createNumber(ret) {
      this.setNumberInput(ret, "min", this.min);
      this.setNumberInput(ret, "max", this.max);
      this.setNumberInput(ret, "initial", this.$refs.initial.value);
      return ret;
    },
    /** @param {Element} ret */
    _createEnum(ret) {
      const initial = this.$refs.initial.value;
      if (initial !== undefined) {
        ret.setAttribute("initial", tvalueToString(initial, "string"));
      }
      ret.setAttribute("values", map(this.enumValues,
        (v) => tvalueToString(v, "string")).join(";"));
      return ret;
    },
    /** @param {Element} ret */
    _createBoolean(ret) {
      ret.setAttribute("initial", this.$refs.initial.editValue);
      return ret;
    },
    /** @param {Element} ret */
    _createAuto(ret) {
      this.setNumberInput(ret, "min", this.min);
      this.setNumberInput(ret, "max", this.max);
      const initial = this.$refs.initial.value;
      if (initial !== undefined) {
        ret.setAttribute("initial", tvalueToString(initial, "string"));
      }
      return ret;
    },
    /**
     * @param {Element} ret
     * @param {string} prop
     * @param {any} value
     */
    setNumberInput(ret, prop, value) {
      if (value !== "") {
        const num = toNumber(value);
        if (!isNaN(num)) {
          ret.setAttribute(prop, toString(num));
        }
      }
    }
  }
});
