// @ts-check

import { mapGetters, mapState } from "vuex";
import { get, noop } from "lodash";
import { getAttribute } from "../../utils/element";
import RelationList from "./RelationList.vue";
import { mixinMaker } from "../../utils";

export default mixinMaker({
  name: "PropertyMixin",
  components: { RelationList },
  props: {
    state: { type: SVGElement, default: null },
    property: { type: SVGElement, default: null }
  },
  computed: {
    /** @return {string} */
    name() { return getAttribute(this.property, "name"); },
    .../** @type {{ value(): any }} */(
      mapState("engine", {
        value: function(state) { return get(state, [ "state", this.name ]); }
      })),
    .../** @type {{ selectedProperty(): Element|null }} */(
      mapState("selection", { selectedProperty: "property" })),
    .../** @type {{ selectedElement(): Element|null }} */(
      mapGetters("selection", [ "selectedElement" ])),
    /** @return {boolean} */
    isSelected() {
      return this.property === this.selectedElement;
    },
    /** @return {boolean} */
    isInSelection() {
      return this.property === this.selectedProperty;
    }
  },
  methods: {
    /** @returns {Promise<void>} */
    async setSelected() {
      await this.$store.dispatch("selection/select", {
        state: this.state, property: this.property, relation: null
      }).catch(noop);
    }
  }
});
