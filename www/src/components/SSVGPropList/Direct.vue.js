// @ts-check

import Vue from "vue";
import BadgeButton from "../BadgeButton.vue";
import RelationMixin from "./RelationMixin";
import { getAttribute } from "../../utils/element";

export default /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "Direct",
  components: { BadgeButton },
  mixins: [ RelationMixin ],
  computed: {
    /** @returns {{ selector: string,  target: string }} */
    badges() {
      return {
        selector: getAttribute(this.relation, "query-selector"),
        target: getAttribute(this.relation, "attribute-name")
      };
    }
  }
});
