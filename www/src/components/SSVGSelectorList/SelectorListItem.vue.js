// @ts-check
import { noop } from "lodash";
import Vue from "vue";
import BadgeButton from "../BadgeButton.vue";

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "SelectorListItem",
  components: { BadgeButton },
  props: {
    isSelected: { type: Boolean, default: false },
    selector: { type: String, default: "" },
    /* eslint-disable-next-line vue/require-prop-types */
    relation: { /* type: SVGElement, VueX typecheck issues */ default: null }
  },
  methods: {
    /** @param {string} value */
    setSelector(value) {
      if (this.relation && value) {
        this.$store.dispatch("edit",
          { what: "relation", values: { "query-selector": value } }).catch(noop);
      }
    }
  }
});
export default component;
