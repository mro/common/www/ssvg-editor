// @ts-check
import Vue from "vue";
import { mapState } from "vuex";
import Card from "../Card.vue";
import Selector from "./SelectorListItem.vue";
import { select } from "d3-selection";
import { get } from "lodash";

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "SSVGSelectorList",
  components: { Card, Selector },
  /** @return {{ selected: string }} */
  data() { return { selected: "" }; },
  computed: {
    ...mapState("engine", {
      directSvg: (/** @type {AppStore.SSVGEngine} */state) => get(state, [ "directEngine", "svg" ]),
      svg: (/** @type {AppStore.SSVGEngine} */state) => get(state, [ "engine", "svg" ])
    }),
    .../** @type {{ svgSelectors(): Set<String>|null }} */mapState("engine", [ "svgSelectors" ]),
    .../** @type {{ relation(): Element|null }} */mapState("selection", [ "relation" ])
  },
  watch: {
    svg() { this.setSelected(this.selected); }
  },
  methods: {
    /** @param {string} value */
    setSelected(value) {
      const svgSelect = select(this.svg);
      const directSvgSelect = select(this.directSvg);
      if (this.selected && value !== this.selected) {
        svgSelect.selectAll(this.selected).classed("x-ssvg-editor-selected", false);
        directSvgSelect.selectAll(this.selected).classed("x-ssvg-editor-selected", false);
      }
      if (value) {
        svgSelect.selectAll(value).classed("x-ssvg-editor-selected", true);
        directSvgSelect.selectAll(value).classed("x-ssvg-editor-selected", true);
      }
      if (value !== this.selected) {
        this.selected = value;
        this.$store.commit("selection/update", { selectorHelper: value });
      }
    },
    /** @param {string} value */
    toggleSelected(value) {
      this.setSelected((this.selected === value) ? "" : value);
    }
  }
});
export default component;
