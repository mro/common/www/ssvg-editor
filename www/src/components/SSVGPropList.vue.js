// @ts-check
import Vue from "vue";
import { first, invoke } from "lodash";

import Card from "./Card.vue";
import Transition from "./SSVGPropList/Transition.vue";
import Property from "./SSVGPropList/Property.vue";
import Computed from "./SSVGPropList/Computed.vue";
import CreatePropertyDialog from "./SSVGPropList/CreatePropertyDialog.vue";
import CreateTransitionDialog from "./SSVGPropList/CreateTransitionDialog.vue";

/**
 * @typedef {typeof import("@cern/base-vue").BaseDialog} BaseDialog
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "SSVGPropList",
  components: { Card, Transition, Property, Computed, CreatePropertyDialog,
    CreateTransitionDialog },
  props: {
    stateElement: { type: SVGElement, default: null }
  },
  /**
   * @return {{
   *  properties: Element[]|null,
   *  computed: Element[]|null,
   *  transition: Element|null }}
   */
  data() {
    return { properties: null, computed: null, transition: null };
  },
  watch: {
    stateElement() { this.load(); }
  },
  mounted() {
    this.load();
  },
  methods: {
    load() {
      this.properties = invoke(this,
        [ "stateElement", "getElementsByTagName" ], "property") ?? null;
      this.computed = invoke(this,
        [ "stateElement", "getElementsByTagName" ], "computed") ?? null;
      const transition = first(this.stateElement?.children) ?? null;
      this.transition = (transition?.tagName === "transition") ? transition : null;
    },
    async setSelected() {
      if (this.$store.state?.selection?.state !== this.stateElement) {
        return await this.$store.dispatch("selection/select",
          { state: this.stateElement });
      }
    },
    async doCreateProperty() {
      try {
        await this.setSelected();
        const prop = await /** @type {V.Instance<BaseDialog>} */(
          this.$root.$refs?.createPropertyDialog)?.request();
        if (!prop) { return; }
        await this.$store.dispatch("add", prop);
      }
      catch (e) {
        console.warn(e);
      }
    },
    async doCreateTransition() {
      try {
        await this.setSelected();
        const prop = await /** @type {V.Instance<BaseDialog>} */(
          this.$root.$refs?.createTransitionDialog)?.request();
        if (!prop) { return; }
        await this.$store.dispatch("add", prop);
      }
      catch (e) {
        console.warn(e);
      }
    }
  }
});
export default component;
