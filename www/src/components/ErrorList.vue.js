// @ts-check
import Vue from "vue";
import { BaseInput } from "@cern/base-vue";


export default /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "ErrorList",
  extends: BaseInput
});
