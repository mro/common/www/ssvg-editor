declare global {
    const TITLE: string;
    const VERSION: string;
    const DEBUG: string;
    const __webpack_public_path__: string; /* eslint-disable-line camelcase */
}

/* typescript declarations for yml imports */
declare module "*.yaml" {
    const data: any;
    export default data;
}

declare module "*.yml" {
    const data: any;
    export default data;
}

declare module "vuex" {
    export * from "vuex/types/index.d.ts";
    export * from "vuex/types/helpers.d.ts";
    export * from "vuex/types/logger.d.ts";
    export * from "vuex/types/vue.d.ts";
}
