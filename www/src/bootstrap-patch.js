// @ts-check

// @ts-ignore
import * as bootstrap from "bootstrap"; // jshint ignore:line

try {
  /* bootstrap4 uses popper1 which has some issues with overflow=hidden and
  small containers, see: https://its.cern.ch/jira/browse/SSVG-20 */
  bootstrap.Dropdown.Default.popperConfig = { positionFixed: true };
  bootstrap.Dropdown.Default.boundary = "window";
}
catch (e) {
  console.warn("failed to patch popper");
}
