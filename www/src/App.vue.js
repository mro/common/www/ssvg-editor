// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import SSVGItem from "./components/SSVGItem.vue";
import SSVGPropList from "./components/SSVGPropList.vue";
import SSVGPropEdit from "./components/SSVGPropEdit.vue";
import SSVGControl from "./components/SSVGControl.vue";
import SSVGSelectorList from "./components/SSVGSelectorList/SSVGSelectorList.vue";
import CreatePropertyDialog from "./components/SSVGPropList/CreatePropertyDialog.vue";
import CreateTransitionDialog from "./components/SSVGPropList/CreateTransitionDialog.vue";

import contributors from "../../Contributors.yml";
import libs from "../../Components.yml";

const options = { contributors, libs };

const component = /** @type {V.Constructor<typeof options, any>} */(Vue).extend({
  name: "App",
  ...options,
  components: { SSVGItem, SSVGPropList, SSVGPropEdit, SSVGControl, SSVGSelectorList, CreatePropertyDialog, CreateTransitionDialog },
  // @ts-ignore
  data() { return { VERSION, TITLE }; },
  /** @type {{ fragment(): Element| null }} */
  computed: {
    ...mapState("engine", [ "stateElements", "fragment" ])
  }
});

export default component;
