// @ts-check
/* eslint-disable max-lines */

import xmldoc from "xmldoc";
import { get, includes, isNil, keys, map, toString, trim } from "lodash";
import { parser } from "@cern/ssvg-engine";

/**
 * @typedef {import("@codemirror/lint").Diagnostic} Diagnostic
 * @typedef {import("@codemirror/lang-xml").ElementSpec} ElementSpec
 */

/**
 * @param {*} err
 */ // @ts-ignore
xmldoc.XmlDocument.prototype._error = function(err) {
  // @ts-ignore
  if (!isNil(this?.parser?.line)) {
    throw { // @ts-ignore
      from: this.parser.position, to: this.parser.position,
      severity: "error",
      message: toString(err)
    };
  }
  throw err;
};

/**
 * @type {{ [name: string]: ElementSpec }}
 */
export const ssvgElements = {
  relation: {
    name: "relation",
    children: [ "transition" ],
    attributes: [
      { name: "query-selector" },
      {
        name: "attribute-name", values: [
          "opacity",
          "width",
          "height",
          "color",
          "background-color",
          "offset",
          "cx",
          "cy",
          "r",
          "radius",
          "rx",
          "ry",
          "rotate",
          "dx",
          "dy",
          "fill",
          "fill-opacity",
          "stroke",
          "stroke-width",
          "scale",
          "x",
          "y",
          "z"
        ]
      },
      { name: "attribute-type", values: [ "auto", "CSS", "XML" ] },
      { name: "from" },
      { name: "to" },
      { name: "values" },
      { name: "key-times" },
      { name: "calc-mode", values: [ "discrete", "linear" ] },
      { name: "onupdate" }
    ]
  },
  transform: {
    name: "transform",
    children: [ "transition" ],
    attributes: [
      { name: "query-selector" },
      { name: "attribute-name", values: [ "transform" ] },
      { name: "attribute-type", values: [ "auto", "CSS", "XML" ] },
      { name: "from" },
      { name: "to" },
      { name: "values" },
      { name: "key-times" },
      { name: "calc-mode", values: [ "discrete", "linear" ] },
      { name: "onupdate" }
    ]
  },
  direct: {
    name: "direct",
    children: [ "transition" ],
    attributes: [
      { name: "query-selector" },
      { name: "attribute-name" },
      { name: "attribute-type", values: [ "auto", "CSS", "XML" ] },
      { name: "onupdate" }
    ]
  },
  property: {
    name: "property",
    children: [ "relation", "transform", "direct" ],
    attributes: [
      { name: "name" },
      { name: "type", values: [ "boolean", "number", "enum", "auto" ] },
      { name: "min" }, { name: "max" },
      { name: "values" },
      { name: "initial" }
    ]
  },
  computed: {
    name: "computed",
    children: [ "relation", "transform", "direct" ],
    attributes: [
      { name: "name" },
      { name: "type", values: [ "boolean", "number", "enum", "auto" ] },
      { name: "min" }, { name: "max" },
      { name: "values" },
      { name: "initial" },
      { name: "onupdate" }
    ]
  },
  transition: {
    name: "transition",
    attributes: [
      { name: "duration" },
      {
        name: "timing-function",
        values: [ "linear", "ease", "ease-in", "ease-out", "ease-in-out", "cubic-bezier" ]
      },
      { name: "delay" },
      { name: "type", values: [ "direct", "strict" ] }
    ]
  }
};

const relativeUnits = new Set([
  "em", "ex", "cap", "ch", "ic", "rem", "lh", "rlh", "vw", "vh", "vi", "vb", "vmin", "vmax"
]);
const absoluteUnits = new Set([
  "cm", "mm", "Q", "in", "pc", "pt", "px"
]);
const angleUnits = new Set([ "deg", "grad", "rad", "turn" ]);
const lengthUnits = new Set([ ...relativeUnits, ...absoluteUnits ]);
const lengthPercentUnits = new Set([ ...lengthUnits, "%" ]);
const numPercentUnits = new Set([ null, "%" ]);

/**
 *
 * @param {ssvg.$TransformRange} transformRange
 * @param {boolean} isCSS true if transform is a CSS transform (SVG transform on the contrary)
 */
export function checkTransformRange(transformRange, isCSS = true) {
  const spec = get(transformInfo, [ transformRange?.transform ]);
  if (!spec) { throw new Error("unknown transform type: " + transformRange?.transform); }

  if (spec.minArgs === spec.maxArgs) {
    if (transformRange.units.length !== spec.minArgs) {
      throw new Error(`transform must have exactly ${spec.minArgs} arguments`);
    }
  }
  else if (transformRange.units.length < spec.minArgs) {
    throw new Error(`transform must have at least ${spec.minArgs} arguments`);
  }
  else if (transformRange.units.length > spec.maxArgs) {
    throw new Error(`transform must have less than ${spec.maxArgs} arguments`);
  }

  /**
   *
   * @param {null | Set<string>} spec
   * @param {string | null} unit
   * @param {number} index
   */
  function checkUnit(spec, unit, index) {
    if (unit) {
      if (spec === null) {
        throw new Error(`transform argument ${index + 1} should have no unit`);
      }
      else if (!spec.has(unit)) {
        throw new Error(`transform argument ${index + 1} has invalid unit "${unit}"`);
      }
    }
    else if (spec && isCSS) {
      throw new Error(`transform argument ${index + 1} should have a valid unit`);
    }
  }

  transformRange.units?.forEach((unit, index) => checkUnit(
    get(spec.units, index, spec.units), unit, index));
}

const transformInfo = {
  // https://www.w3.org/TR/css-transforms-1/#typedef-transform-function
  matrix: { minArgs: 1, maxArgs: 6, units: null },
  translate: { minArgs: 1, maxArgs: 2, units: lengthPercentUnits },
  translateX: { minArgs: 1, maxArgs: 1, units: lengthPercentUnits },
  translateY: { minArgs: 1, maxArgs: 1, units: lengthPercentUnits },
  scale: { minArgs: 1, maxArgs: 2, units: null },
  scaleX: { minArgs: 1, maxArgs: 1, units: null },
  scaleY: { minArgs: 1, maxArgs: 1, units: null },
  rotate: { minArgs: 1, maxArgs: 1, units: angleUnits },
  skew: { minArgs: 1, maxArgs: 2, units: angleUnits },
  skewX: { minArgs: 1, maxArgs: 1, units: angleUnits },
  skewY: { minArgs: 1, maxArgs: 1, units: angleUnits },
  // https://drafts.csswg.org/css-transforms-2/#three-d-transform-functions
  matrix3d: { minArgs: 1, maxArgs: 6, units: null },
  translate3d: { minArgs: 3, maxArgs: 3, units: [ lengthPercentUnits, lengthPercentUnits, lengthUnits ] },
  translateZ: { minArgs: 1, maxArgs: 1, units: lengthUnits },
  scale3d: { minArgs: 3, maxArgs: 3, units: numPercentUnits },
  scaleZ: { minArgs: 1, maxArgs: 1, units: numPercentUnits },
  rotate3d: { minArgs: 3, maxArgs: 4, units: [ null, null, null, angleUnits ] },
  rotateX: { minArgs: 1, maxArgs: 1, units: angleUnits },
  rotateY: { minArgs: 1, maxArgs: 1, units: angleUnits },
  rotateZ: { minArgs: 1, maxArgs: 1, units: angleUnits }
  // "perspective": "perspective(0); perspective(100px)",
};

export const transformList = keys(transformInfo);

export class SSVGLinter {
  /**
   * @param {string} data
   */
  constructor(data) {
    this.data = data;
    /** @type {Diagnostic[]} */
    this.diagnostics = [];
  }

  /**
   * @returns {Diagnostic[]}
   */
  lint() {
    try {
      const doc = new xmldoc.XmlDocument(this.data);
      if (includes([ "transform", "relation", "direct" ], doc.name)) {
        this.checkAttrRequired(doc, "query-selector");
        this.checkAttrValues(doc, "attribute-type", [ "auto", "CSS", "XML" ], false);
        if (doc.name !== "direct") {
          this.checkAttrValues(doc, "calc-mode", [ "discrete", "linear" ], false);
          if (doc.name === "relation") {
            this.checkTValues(doc);
          }
          else if (doc.name === "transform") {
            this.checkTTransform(doc);
          }
        }
      }
    }
    catch (/** @type {any} */ e) {
      if (isNil(e?.from)) {
        return [ { from: 0, to: 0, severity: "error", message: toString(e) } ];
      }
      return [ e ];
    }
    return this.diagnostics;
  }

  /**
   * @param {xmldoc.XmlElement} node
   * @param {string} attr
   */
  attrPosition(node, attr) {
    const idx = this.data.indexOf(`${attr}=`, node.startTagPosition);
    return (idx >= 0) ? idx : node.startTagPosition;
  }

  /**
   * @param {xmldoc.XmlElement} node
   * @param {string} name
   */
  checkAttrRequired(node, name, allowEmpty = false) {
    if ((allowEmpty) ? isNil(node.attr[name]) : !node.attr[name]) {
      this.diagnostics.push({
        from: node.startTagPosition, to: node.startTagPosition,
        severity: "error",
        message: `"${name}" attribute missing`
      });
      return false;
    }
    return true;
  }

  /**
   * @param {xmldoc.XmlElement} node
   * @param {string[]} values
   * @param {string} name
   */
  checkAttrValues(node, name, values, required = true) {
    if (required && !this.checkAttrRequired(node, name, false)) { return false; }

    const value = node.attr[name];
    if (!isNil(value) && !includes(values, value)) {
      const pos = this.attrPosition(node, name);
      this.diagnostics.push({
        from: pos, to: pos,
        severity: "error",
        message: `invalid "${name}", allowed-values: [ "${values.join('", "')}" ]`
      });
      return false;
    }
    return true;
  }

  /**
   * @param {xmldoc.XmlElement} node
   */
  checkTValues(node) {
    if (node.attr["values"]) {
      try {
        parser.parseTRange(node.attr["values"]);
      }
      catch (e) {
        const pos = this.attrPosition(node, "values");
        this.diagnostics.push({
          from: pos, to: pos, severity: "error",
          message: "failed to parse \"values\": " + toString(e)
        });
        return false;
      }
    }
    else if (node.attr["to"]) {
      try {
        if (node.attr["from"]) {
          parser.makeRange(parser.parseTValue(node.attr["from"]),
            parser.parseTValue(node.attr["to"]));
        }
        else {
          parser.parseTValue(node.attr["to"]);
        }
      }
      catch (e) {
        const pos = this.attrPosition(node, "to");
        this.diagnostics.push({
          from: pos, to: pos, severity: "error",
          message: "failed to parse \"from/to\": " + toString(e)
        });
        return false;
      }
    }
    else if (node.attr["by"]) {
      try {
        parser.parseTValue(node.attr["by"]);
      }
      catch (e) {
        const pos = this.attrPosition(node, "by");
        this.diagnostics.push({
          from: pos, to: pos, severity: "error",
          message: "failed to parse \"by\": " + toString(e)
        });
        return false;
      }
    }
    else {
      this.diagnostics.push({
        from: node.startTagPosition, to: node.startTagPosition,
        severity: "error",
        message: '"to"/"from", "by" or "values" attribute missing'
      });
      return false;
    }
    return true;
  }


  /**
   * @param {xmldoc.XmlElement} node
   */ /* eslint-disable-next-line complexity */
  checkTTransform(node) {
    /** @type {ssvg.$Transform[][]} */
    let transforms = [];
    if (node.attr["values"]) {
      try {
        transforms = parser.parseTTransformRange(node.attr["values"]) ?? [];
      }
      catch (e) {
        const pos = this.attrPosition(node, "values");
        this.diagnostics.push({
          from: pos, to: pos, severity: "error",
          message: "failed to parse \"values\": " + toString(e)
        });
        return false;
      }
    }
    else if (node.attr["to"]) {
      try {
        if (node.attr["from"]) {
          transforms.push(parser.parseTTransform(node.attr["from"]) ?? []);
        }
        transforms.push(parser.parseTTransform(node.attr["to"]) ?? []);
      }
      catch (e) {
        const pos = this.attrPosition(node, "to");
        this.diagnostics.push({
          from: pos, to: pos, severity: "error",
          message: "failed to parse \"from/to\": " + toString(e)
        });
        return false;
      }
    }
    else if (node.attr["by"]) {
      try {
        transforms.push(parser.parseTTransform(node.attr["by"]) ?? []);
      }
      catch (e) {
        const pos = this.attrPosition(node, "by");
        this.diagnostics.push({
          from: pos, to: pos, severity: "error",
          message: "failed to parse \"by\": " + toString(e)
        });
        return false;
      }
    }
    else {
      this.diagnostics.push({
        from: node.startTagPosition, to: node.startTagPosition,
        severity: "error",
        message: '"to"/"from", "by" or "values" attribute missing'
      });
      return false;
    }
    try {
      const trange = parser.normalizeTransform(transforms);
      try {

        /* auto mode depends on attribute-name */
        const isCSS = (node.attr["attribute-type"] === "CSS") ||
          ((get(node.attr, "attribute-type", "auto") === "auto") && !!node.attr["attribute-name"]);
        trange?.forEach((trange) => checkTransformRange(trange, isCSS));
      }
      catch (e) {
        this.diagnostics.push({
          from: node.startTagPosition, to: node.startTagPosition,
          severity: "warning",
          message: toString(e)
        });
      }
    }
    catch (e) {
      this.diagnostics.push({
        from: node.startTagPosition, to: node.startTagPosition,
        severity: "error",
        message: "failed to normalize transform: " + toString(e)
      });
      return false;
    }
    return true;
  }
}

/**
 *
 * @param {string} data
 * @return {Diagnostic[]}
 */
export function ssvgLint(data) {
  const linter = new SSVGLinter(data);
  return linter.lint();
}

const strRe = /\s*([^\s]*)[=]"([^"]*)\s*"/g;
/**
* @param  {string} text
* @return {string}
*/
export function ssvgPrettyPrint(text) {
  return text
  .replace(strRe, (m, key, value) => {
    if (value.length > 20 && includes([ "key-times", "values" ], key)) {
      const valueIndent = " ".repeat(key.length + 4);
      const values = map(value.split(";"), (v) => trim(v));
      return `\n  ${key}="${values.join(";\n" + valueIndent)}"`;
    }
    else {
      return `\n  ${key}="${value}"`;
    }
  });
}
