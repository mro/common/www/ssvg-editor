// @ts-check
import {
  clone, cloneDeep, find, get, identity, isNil, map, padStart,
  size, toNumber, toString
} from "lodash";
import { parser } from "@cern/ssvg-engine";
import { getAttribute } from "./element";
import { color, rgb } from "d3-color";
import { BaseLogger as logger } from "@cern/base-vue";

/**
 * @param  {Element|null} stateElement
 * @param  {string} name
 * @return {Element|null}
 */
export function getPropertyByName(stateElement, name) {
  return find(get(stateElement, "children"), (c) => {
    return (c.tagName === "property" || c.tagName === "computed") &&
      c.getAttribute("name") === name;
  }) || null;
}

/**
 * @param {Element|null} transformElement
 */
export function getTransformValues(transformElement) {
  // FIXME: support "by" and "to" without "from" ? (requires svg and selector access)
  try {
    /** @type {ssvg.$Transform[][]|null} */
    let transforms = [];
    const values = getAttribute(transformElement, "values");
    if (values) {
      transforms = parser.parseTTransformRange(values);
    }
    else {
      const from = getAttribute(transformElement, "from");
      if (from) {
        transforms.push(parser.parseTTransform(from) ?? []);
      }
      const to = getAttribute(transformElement, "to");
      if (to) {
        transforms.push(parser.parseTTransform(from) ?? []);
      }
    }
    return parser.normalizeTransform(transforms ?? []);
  }
  catch (err) {
    // logger.error(err);
    return null;
  }
}

/**
 * @param  {Element|null} relationElement
 */
export function getRelationValues(relationElement) {
  try {
    const values = getAttribute(relationElement, "values");
    if (!isNil(values)) {
      return parser.parseTRange(values);
    }
    const to = getAttribute(relationElement, "to");
    if (!isNil(to)) {
      const from = getAttribute(relationElement, "from");
      if (isNil(from)) {
        // FIXME get initial value from store
      }
      return parser.makeRange(
        parser.parseTValue(from), parser.parseTValue(to));
    }
    else {
      const by = getAttribute(relationElement, "by");
      if (!isNil(by)) {
        const current = parser.parseTValue(""); // FIXME get initial value from store
        const to = parser.parseTValue(getAttribute(relationElement, "by"));
        /* @ts-ignore */
        to.value += current?.value ?? 0;
        return parser.makeRange(current, to);
      }
    }
  }
  catch (err) {
    logger.error(err);
  }
  return null;
}

/**
 * @param {Element|null} relationElement relation or transform
 */
export function getKeyTimes(relationElement) {
  const keyTimes = getAttribute(relationElement, "key-times");
  if (!isNil(keyTimes)) {
    return parser.parseTRange(keyTimes)?.values ?? null;
  }
  return null;
}

const strRe = /([\\"'])/g;
/**
 * @param  {any} value
 * @param  {string} type
 */
export function tvalueToString(value, type) {
  const valueString = toString(value);
  if (type === "string") {
    return `"${valueString.replace(strRe, "\$1")}"`;
  }
  return valueString;
}


/**
 * @type {{ [from: string]: { [to: string]: (v: any) => any } }}
 */
const typeConverter = {
  string: {
    number: (v) => (toNumber(v.slice(1, -1)) || 0),
    color: (v) => {
      v = v.slice(1, -1);
      return color(v) ? (v) : ("#" + padStart((toNumber(v) || 0).toString(16), 6, "0"));
    }
  },
  number: {
    string: (v) => `"${v}"`,
    color: (v) => ("#" + padStart(toNumber(v).toString(16), 6, "0"))
  },
  color: {
    string: (v) => `"${(color(v) || rgb(0, 0, 0)).toString()}"`,
    number: (v) => {
      const c = (color(v) || rgb(0, 0, 0)).rgb();
      return c.b + (c.g << 8) + (c.r << 16);
    }
  }
};

/**
 * @brief convert a TRange to another type
 * @param  {ssvg.$TRange} tvalues
 * @param  {ssvg.$TTypeName} type
 * @return {ssvg.$TRange}
 */
export function trangeConvert(tvalues, type) {
  tvalues = cloneDeep(tvalues);
  const convert = get(typeConverter, [ tvalues.type, type ], identity);
  tvalues.values = map(tvalues.values, (v) => convert(v));
  delete tvalues.unit;
  tvalues.type = type;
  return tvalues;
}

/**
 * @param  {number[]} args
 * @param  {(string|null)[]} units
 * @return {string}
 */
export function transformArgsStr(args, units) {
  /** @type {any[]} */
  const ret = clone(args);
  if (!ret) { return ""; }
  for (let i = 0; i < size(units); ++i) {
    if (units[i]) {
      ret[i] = ret[i] + units[i];
    }
  }
  return ret.join(", ");
}
