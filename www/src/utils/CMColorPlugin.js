// @ts-check
import { syntaxTree } from "@codemirror/language";
import { Decoration, EditorView, ViewPlugin, WidgetType } from "@codemirror/view";
import { color } from "d3-color";

/**
 * @typedef {import("@codemirror/state").Range<Decoration>} RangeDecoration
 */

export const ColorNames = [
  "aliceblue",
  "antiquewhite",
  "aqua",
  "aquamarine",
  "azure",
  "beige",
  "bisque",
  "black",
  "blanchedalmond",
  "blue",
  "blueviolet",
  "brown",
  "burlywood",
  "cadetblue",
  "chartreuse",
  "chocolate",
  "coral",
  "cornflowerblue",
  "cornsilk",
  "crimson",
  "cyan",
  "darkblue",
  "darkcyan",
  "darkgoldenrod",
  "darkgray",
  "darkgreen",
  "darkgrey",
  "darkkhaki",
  "darkmagenta",
  "darkolivegreen",
  "darkorange",
  "darkorchid",
  "darkred",
  "darksalmon",
  "darkseagreen",
  "darkslateblue",
  "darkslategray",
  "darkslategrey",
  "darkturquoise",
  "darkviolet",
  "deeppink",
  "deepskyblue",
  "dimgray",
  "dimgrey",
  "dodgerblue",
  "firebrick",
  "floralwhite",
  "forestgreen",
  "fuchsia",
  "gainsboro",
  "ghostwhite",
  "gold",
  "goldenrod",
  "gray",
  "green",
  "greenyellow",
  "grey",
  "honeydew",
  "hotpink",
  "indianred",
  "indigo",
  "ivory",
  "khaki",
  "lavender",
  "lavenderblush",
  "lawngreen",
  "lemonchiffon",
  "lightblue",
  "lightcoral",
  "lightcyan",
  "lightgoldenrodyellow",
  "lightgray",
  "lightgreen",
  "lightgrey",
  "lightpink",
  "lightsalmon",
  "lightseagreen",
  "lightskyblue",
  "lightslategray",
  "lightslategrey",
  "lightsteelblue",
  "lightyellow",
  "lime",
  "limegreen",
  "linen",
  "magenta",
  "maroon",
  "mediumaquamarine",
  "mediumblue",
  "mediumorchid",
  "mediumpurple",
  "mediumseagreen",
  "mediumslateblue",
  "mediumspringgreen",
  "mediumturquoise",
  "mediumvioletred",
  "midnightblue",
  "mintcream",
  "mistyrose",
  "moccasin",
  "navajowhite",
  "navy",
  "oldlace",
  "olive",
  "olivedrab",
  "orange",
  "orangered",
  "orchid",
  "palegoldenrod",
  "palegreen",
  "paleturquoise",
  "palevioletred",
  "papayawhip",
  "peachpuff",
  "peru",
  "pink",
  "plum",
  "powderblue",
  "purple",
  "rebeccapurple",
  "red",
  "rosybrown",
  "royalblue",
  "saddlebrown",
  "salmon",
  "sandybrown",
  "seagreen",
  "seashell",
  "sienna",
  "silver",
  "skyblue",
  "slateblue",
  "slategray",
  "slategrey",
  "snow",
  "springgreen",
  "steelblue",
  "tan",
  "teal",
  "thistle",
  "tomato",
  "turquoise",
  "violet",
  "wheat",
  "white",
  "whitesmoke",
  "yellow",
  "yellowgreen"
];
export const HexToName = new Map();
ColorNames.forEach((c) => HexToName.set(color(c)?.formatHex(), c));

class ColorPickerPlugin {
  /** @param {EditorView} view */
  constructor(view) {
    this.state = new WeakMap();
    this.decorations = this.makeDecorations(view);
  }

  /**
   * @param {import("@codemirror/view").ViewUpdate} update
   */
  update(update) {
    if (update.docChanged || update.viewportChanged) {
      this.decorations = this.makeDecorations(update.view);
    }
  }

  /** @param {EditorView} view */
  makeDecorations(view) {
    /** @type {RangeDecoration[]} */
    const widgets = [];
    for (const range of view.visibleRanges) {
      syntaxTree(view.state).iterate({
        from: range.from,
        to: range.to,
        enter: ({ type, from, to }) => { /* jshint ignore:line */
          if (type.name !== "Attribute") { return; }
          const value = view.state.doc.sliceString(from, to);

          if (value.startsWith("from=") || value.startsWith("to=")) {
            const start = value.indexOf("\"") + 1;
            const end = value.lastIndexOf("\"");
            const c = color(value.slice(start, end));
            if (c) {
              const widget = Decoration.widget({
                widget: new ColorPickerWidget({
                  plugin: this, color: c, from: from + start, to: from + end
                })
              });
              widgets.push(widget.range(from + end));
            }
          }
          else if (value.startsWith("values=")) {
            let start = value.indexOf("\"") + 1;
            const values = value.slice(start, value.lastIndexOf("\"")).split(";");
            values.forEach((v) => {
              const end = start + v.length;
              const c = color(value.slice(start, start + v.length));
              if (c) {
                const widget = Decoration.widget({
                  widget: new ColorPickerWidget({
                    plugin: this, color: c, from: from + start, to: from + end
                  })
                });
                widgets.push(widget.range(from + end));
              }
              start += v.length + 1;
            });
          }
        }
      });
    }
    return Decoration.set(widgets);
  }
}

const colorPickerPlugin = ViewPlugin.fromClass(ColorPickerPlugin, {
  decorations: (v) => v.decorations,
  eventHandlers: {
    change(e, view) {
      const target = /** @type {HTMLInputElement} */ (e.target);
      if (target?.nodeName === "INPUT" &&
        target.parentElement?.classList.contains(wrapperClassName)) {
        /** @type {ColorPickerWidget} */
        const picker = this.state.get(target.parentElement);
        const value = HexToName.get(color(target.value)?.formatHex()) ?? target.value;
        view.dispatch({ changes: { from: picker.from, to: picker.to, insert: value } });
      }
    }
  }
});

const wrapperClassName = "cm-color-picker-wrapper";
class ColorPickerWidget extends WidgetType {
  /**
   * @param  {{ plugin: ColorPickerPlugin, color: ReturnType<color>, from: number, to: number }} kwargs
   */
  constructor(kwargs) {
    super();
    this.plugin = kwargs.plugin;
    this.color = kwargs.color;
    this.from = kwargs.from;
    this.to = kwargs.to;
  }

  /**
   * @param {ColorPickerWidget} other
   */
  eq(other) {
    return (
      other.color === this.color &&
      other.from === this.from &&
      other.to === this.to);
  }

  toDOM() {
    const picker = document.createElement("input");
    picker.type = "color";
    picker.value = this.color.formatHex();
    const wrapper = document.createElement("span");
    wrapper.appendChild(picker);
    wrapper.className = wrapperClassName;
    this.plugin.state.set(wrapper, this);
    return wrapper;
  }

  ignoreEvent() {
    return false;
  }

  /** @param {HTMLElement} dom */
  destroy(dom) {
    this.plugin.state.delete(dom);
  }
}


const colorPickerTheme = EditorView.baseTheme({
  [`.${wrapperClassName}`]: {
    display: "inline-block",
    outline: "1px solid #00000040",
    marginLeft: "0.3em",
    height: "1em",
    width: "1em",
    borderRadius: "1px",
    verticalAlign: "middle",
    marginTop: "-2px"
  },
  [`.${wrapperClassName} input[type="color"]`]: {
    cursor: "pointer",
    height: "100%",
    width: "100%",
    padding: 0,
    border: "none",
    "&::-webkit-color-swatch-wrapper": {
      padding: 0
    },
    "&::-webkit-color-swatch": {
      border: "none"
    },
    "&::-moz-color-swatch": {
      border: "none"
    }
  }
});
export default [ colorPickerPlugin, colorPickerTheme ];
