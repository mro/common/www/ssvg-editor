// @ts-check
import { invoke, isNil } from "lodash";

/**
 * @param  {Element|null} elt
 * @param  {string} name
 * @return {string}
 */
export function getAttribute(elt, name) {
  return invoke(elt, "getAttribute", name);
}

/**
 * @brief set or remove attribute
 * @param {Element|null} elt
 * @param {string} name
 * @param {string|null|undefined} value
 */
export function setAttribute(elt, name, value) {
  if (isNil(value)) {
    invoke(elt, "removeAttribute", name);
  }
  else {
    invoke(elt, "setAttribute", name, value);
  }
}
