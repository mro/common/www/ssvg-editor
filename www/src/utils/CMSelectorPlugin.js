// @ts-check
import { syntaxTree } from "@codemirror/language";
import { Decoration, EditorView, ViewPlugin, WidgetType } from "@codemirror/view";
import { BaseLogger as logger } from "@cern/base-vue";

/**
 * @typedef {import("@codemirror/state").Range<Decoration>} RangeDecoration
 */

class SelectorPlugin {
  /** @param {EditorView} view */
  constructor(view) {
    this.state = new WeakMap();
    this.decorations = this.makeDecorations(view);
  }

  /**
   * @param {import("@codemirror/view").ViewUpdate} update
   */
  update(update) {
    if (update.docChanged || update.viewportChanged) {
      this.decorations = this.makeDecorations(update.view);
    }
  }

  /** @param {EditorView} view */
  makeDecorations(view) {
    /** @type {RangeDecoration[]} */
    const widgets = [];
    for (const range of view.visibleRanges) {
      syntaxTree(view.state).iterate({
        from: range.from,
        to: range.to,
        /* @ts-ignore */
        enter: ({ type, from, to }) => { /* jshint ignore:line */
          if (type.name !== "Attribute") { return; }
          const value = view.state.doc.sliceString(from, to);
          if (!value.startsWith("query-selector=\"")) { return; }

          const start = value.indexOf("\"") + 1;
          const end = value.lastIndexOf("\"");
          const widget = Decoration.widget({
            widget: new SelectorWidget({
              plugin: this, from: from + start, to: from + end
            })
          });
          widgets.push(widget.range(from + end));
        }
      });
    }
    return Decoration.set(widgets);
  }
}

export const makeSelectorPlugin = (/** @type {() => Promise<string>} */ callback) => ViewPlugin.fromClass(SelectorPlugin, {
  decorations: (v) => v.decorations,
  eventHandlers: {
    click(e, view) {
      const target = /** @type {HTMLElement} */ (e.target);
      if (target.parentElement?.classList.contains(wrapperClassName)) {
        /** @type {SelectorWidget} */
        const widget = this.state.get(target.parentElement);

        // click event handler must return a boolean or void (not a promise)
        (async () => await callback()
        .then((value) => view.dispatch({
          changes: { from: widget.from, to: widget.to, insert: value }
        }),
        (e) => logger.error(e)))();
      }
    }
  }
});

const wrapperClassName = "cm-selector-wrapper";
class SelectorWidget extends WidgetType {
  /**
   * @param  {{ plugin: SelectorPlugin, from: number, to: number }} kwargs
   */
  constructor(kwargs) {
    super();
    this.plugin = kwargs.plugin;
    this.from = kwargs.from;
    this.to = kwargs.to;
  }

  /**
   * @param {SelectorWidget} other
   */
  eq(other) {
    return (
      other.from === this.from &&
      other.to === this.to);
  }

  toDOM() {
    const elt = document.createElement("i");
    elt.classList.add("fa", "fa-crosshairs");
    const wrapper = document.createElement("span");
    wrapper.appendChild(elt);
    wrapper.className = wrapperClassName;
    this.plugin.state.set(wrapper, this);
    return wrapper;
  }

  ignoreEvent() {
    return false;
  }

  /** @param {HTMLElement} dom */
  destroy(dom) {
    this.plugin.state.delete(dom);
  }
}

export const selectorTheme = EditorView.baseTheme({
  [`.${wrapperClassName}`]: {
    cursor: "pointer",
    // display: "inline-block",
    // outline: "1px solid #00000040",
    marginLeft: "0.3em",
    // height: "1em",
    // width: "1em",
    // borderRadius: "1px",
    verticalAlign: "middle"
    // marginTop: "-2px"
  }
});

export default makeSelectorPlugin;
