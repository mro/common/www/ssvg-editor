# SSVG Editor WebSite

This application is available [there](https://mro-dev.web.cern.ch/ssvg-editor) and a [beta](https://mro-dev.web.cern.ch/beta/ssvg-editor) is also available.

# Documentation

Please refer to the [SSVG specification](https://apc-dev.web.cern.ch/docs/std/ssvg-specification.html).

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:
```bash
cd deploy

# Add correct passwords in ConfigMap, replacing xxx
# Do never commit passwords !
vim ssvg-editor-beta.yaml

oc apply -f ssvg-editor-beta.yaml
```
